/**
@file Source.cpp
@brief uses CovidGraph.h to simulate network model, also requires CovidGraph.cpp
@author Michael Lindstom, corresponding author: m i k e l [at] m a t h [dot] u c l a [dot] e d u
@author Thomas Valles
@date Dec 2020
@version 2.0

@note C++17 is required

@details see CovidGraph.h
*/

#include "CovidGraph.h"
#include<iostream>
#include<fstream>
#include<thread>
#include<cmath>
#include<string>
#include<numeric>

/**
This program can either be run alone as an executable or with some user-defined settings.

Upom compiling, with no arguments:
- "parameters.txt" must be a local text file with the parameters
- it will simulate for 300 days
- generating 4 simulations
- each set of results prefixed by "sim"

The program can also be run by giving it 4 arguments. For example, if the program is called
'covid' then:

./covid n-100 e-150 d-params.txt o-out

while call the program with 100 trials, ending on day 150, with a parameter file "params.txt"
and all output files beginning with "out". In general, the flags are:

n-[NUMBER OF TRIALS]
e-[DAY TO END]
d-[NAME OF DATA FILE WITH PARAMETERS]
o-[PREFIX FOR NAME OF OUTPUT FILES]
*/
int main(int argc, char** argv) {

	// flags if bad things happen
	static constexpr int arg_count_wrong = -1;
	static constexpr int arg_format_wrong = -2;

	// proper arg counts
	static constexpr int default_count = 1;
	static constexpr int normal_count = 5;

	// settings
	const char* input_file_ptr = nullptr; // data file
	const char* num_ptr = nullptr; // number of trials to simulate
	const char* output_base_ptr = nullptr; // base name for output files
	const char* end_day_ptr = nullptr; // day when simulations end

	if (argc == normal_count) { // standard run, use defaults
		for (int i = 1; i < normal_count; ++i) { // first argument is always the file so start at 1
			switch (argv[i][0]) { // e = end day, n = number trials, d = data (input) file, o = output prefix
			case 'e': end_day_ptr = argv[i] + 2; break;
			case 'n': num_ptr = argv[i] + 2; break;
			case 'd': input_file_ptr = argv[i] + 2; break;
			case 'o': output_base_ptr = argv[i] + 2; break;
			default: std::cerr << "Invalid arguments supplied!\n"; return arg_format_wrong; break;
			}
		}
	}
	else if (argc == default_count) { // use some default choices
		end_day_ptr = "300";
		num_ptr = "4";
		input_file_ptr = "parameters.txt";
		output_base_ptr = "sim";
	}
	else { // bad number of arguments
		std::cout << "bad argument count";
		return arg_count_wrong;
	}

	constexpr double dT = 1;  // time interval in days 
	const unsigned num_days = std::stoul(end_day_ptr); // day to end
	const std::string param_file = input_file_ptr; // where parameters found
	const std::string file_base = output_base_ptr; // what to write to
	const unsigned num_sims = std::stoul(num_ptr); // how many trials to un


	std::cout << "Reading data" << '\n';
	auto settings = covid::read_parameters(param_file);

	// review settings
	std::cout << "Simulations to begin.\n";
	std::cout << "using " << param_file << " for parameters,\n";
	std::cout << "simulating for " << num_days << " days,\n";
	std::cout << "to produce " << num_sims << " number of result files prefixed by " << file_base << ".\n";

	const unsigned max_threads = std::thread::hardware_concurrency();
	std::cout << "Can use at most " << max_threads << " threads." << '\n';

	const unsigned chunk_size = 1 + num_sims / max_threads; // integer value of how many files a thread should do 

	// store all the threads in a vector to later join
	std::vector< std::unique_ptr< std::thread > > threads;

	std::cout << "Doing parallel simulations" << '\n';
	// need to delegate chunks to max possible number of threads
	for (int thread_count = 0; thread_count <= max_threads; ++thread_count) {
		int low = thread_count * chunk_size;
		int up = (thread_count + 1) * chunk_size - 1; // max possible upper
		up = ((up >= num_sims) ? num_sims - 1 : up); // don't go past number of files
		threads.emplace_back(
			std::make_unique<std::thread>(covid::run_simulations, std::cref(file_base), low, up,
				std::cref(settings), dT, num_days));
	}
	for (auto& t : threads) { // join all the threads
		t->join();
	}

	std::cout << "simulations are now done!" << '\n';

	return 0;
}

