/**
@file CovidGraph.h
@brief header file, to be accompanied by CovidGraph.cpp to simulate network model
@author Michael Lindstom, corresponding author: m i k e l [at] m a t h [dot] u c l a [dot] e d u
@author Thomas Valles
@date Sept 2021
@version 3.0

@note C++17 is required

@details This file and the accompanying cpp file and source file can be used to simulate a
network model that includes 4 classes of people: caregivers, disabled, essential workers, general;
6 COVID-19 states: susceptible, exposed, asymptomatic, symptomatically ill, hospitalized, and recovered;
3 different types of interaction risk weightings for: social contacts, family contacts, and caregiving relations.
The different parameters are set in a parameter file and used in Source.cpp
*/

#ifndef _COVID__GRAPH_
#define _COVID__GRAPH_

#include<unordered_set>
#include<mutex>
#include<vector>
#include<functional>
#include<cstddef>
#include<iostream>
#include<memory>
#include<random>
#include<string>
#include<thread>
#include<type_traits>
#include<numeric>
#include<sstream>
#include<map>
#include<optional>
#include<unordered_set>


namespace covid {

	/**
	@enum state represents the disease state
	*/
	enum class state {
		S, // susceptible
		E, // exposed 
		A, // asymptomatic
		I, // ill
		H, // hospital
		R // removed 
	};

	/**
	@struct state_counts stores counts of people in each group
	*/
	struct state_counts {
		// S = Susceptible, E = exposed, A = Asymptomatic, I = Ill, H = Hosptitalized, R = Removed
		size_t S, E, A, I, H, R;
	};

	/**
	@struct group_states gets the state counts for all groups
	*/
	struct group_states {
		state_counts general, caregivers, clients, essential;
	};

	/**
	@enum Open_Close enumerates closing down or opening up conditions
	*/
	enum class Open_Close {
		OPEN,
		CLOSE
	};


	/**
	@class integer_distribution  represents a distribution we use to produce nonnegative  integers
	@tparam Int an unsigned integer type to generate
	@tparam Float, a floating point type to specify the precision
	*/
	template<typename Int, typename Float,
		typename = std::enable_if_t< std::is_unsigned_v<Int>&& std::is_floating_point_v<Float>, void* >>
		class integer_distribution {

		protected:
			/**
			retrives the eng to be used in calls in derived classes
			@return the random engine
			*/
			std::default_random_engine& get_eng() const {
				static thread_local std::random_device rd;
				static thread_local std::default_random_engine eng(rd()); // to use in calls
				return eng;
			}

		public:
			/**
			Should have a call operator to return an integer
			@return an Int sampled from a distribution
			*/
			virtual Int operator()() const = 0;

			/**
			Should compute the mean of the distribution
			@return the mean
			*/
			virtual Float get_mean() const = 0;

			/**
			destructor. don't need to re-enable copy/moves for this class
			*/
			virtual ~integer_distribution() = default;

			/**
			equality operator: virtual to avoid some headaches, unfortunately not symmetric
			@param other the other distribution to compare with
			@return if the two are equal
			*/
			virtual bool equal(const integer_distribution& other) const {
				return false;
			}
	};

	/**
	@class discrete_distribution generates integer values for N where Pr(N=0), Pr(N=1), etc., are specified
	@tparam Int an unsigned integer type to generate
	@tparam Float the type of precision used in calculations
	*/
	template<typename Int, typename Float>
	class discrete_distribution : public integer_distribution<Int, Float> {

	private:
		std::vector<Float> probs; // the probabilities of 0, 1, 2, ...	
		Float mean; // mean of the distribution
	public:

		/**
		makes a discrete distribution with a set of counts/weights
		@param counts a vector storing the counts/probabilities of counts 0 and up consecutively
		and if nothing is provided, it will always return 0
		*/
		discrete_distribution(std::vector<Float> counts = {}) : probs(std::move(counts)), mean(0) {

			// important to use probs here: counts will be in undefined state due to the move 

			Float total = std::accumulate(std::begin(probs), std::end(probs),
				static_cast<Float>(0));

			Int hsize = 0; // household size

			Float weighted = std::accumulate(std::begin(probs), std::end(probs),
				static_cast<Float>(0),
				// adds the numbers but weights by household size, mutable to allow hsize to change
				[hsize](Float f1, Float f2) mutable -> Float {
					return f1 + f2 * (hsize++);
				});

			mean = weighted / total; // calculate the mean

			for (Float& f : probs) { // normalize now
				f /= total;
			}
		}

		/**
		returns the mean
		@return the distribution mean
		*/
		Float get_mean() const override {
			return mean;
		}

		/**
		gives us a random Int
		@return an Int N sampled with Pr(N=0) = probs[0], Pr(N=1) = probs[1], etc.
		*/
		Int operator()() const override {

			if (probs.empty()) { // for empty, just return 0
				return 0;
			}

			static thread_local std::uniform_real_distribution<Float> u{ 0, 1 };

			// need cumulative mass to reach/exceed cdf
			Float cdf = u(integer_distribution<Int, Float>::get_eng());

			size_t i = 0; // place in vector 

			while (cdf > 0) { // until accumulated enough mass
				cdf -= probs[i++];
			}

			// i will always be at least 1, hence this is safe
			return static_cast<Int>(i - 1);
		}

		/**
		returns the parameters
		@return a tuple of the mean and vector of counts
		*/
		auto get_params() const {
			return std::tie(mean, probs);
		}


		/**
		determines if this distribution equals another
		@param other the distribution to compare against
		@return if they are equal in both type of distribution (discrete_distribution) and parameters
		*/
		bool equal(const integer_distribution<Int, Float>& other) const override {
			// they must of of the same type, in which case we can just compare parameters
			if (auto p = dynamic_cast<const discrete_distribution*>(&other)) {
				return get_params() == p->get_params();
			}
			return false;
		}

	};


	/**
	@class power_pmf generates numbers with a distribution ~1/n^p for large n
	@tparam Int the integer type to return
	@tparam Float the floating point type parameterizing the distribution
	*/
	template<typename Int, typename Float>
	class power_pmf : public integer_distribution<Int, Float> {

	private:
		Float p; // power law
		Int low, high; // lowest and highest values to return
		Float mean;

		/**
		This function sets the mean for the class
		*/
		void set_mean() {
			Float low_temp = low, high_temp = high;

			if (low == 0) { // will need to shift back afterwards
				low_temp = 1;
				high_temp = static_cast<Float>(high) + 1;
			}

			Float norm_const = 1; // normalizing constant

			if (p == 1) { // integrating 1/x
				norm_const = 1 / std::log((high_temp + 1) / low_temp);
			}
			else { // integrating 1/x^p, p != 1
				norm_const = (1 - p) / (std::pow(high_temp + 1, 1 - p) - std::pow(low_temp, 1 - p));
			}

			if (p == 1) { // special case of summing n log((n+1)/n)
				auto indef = [](Float x)->Float {
					return 0.5 * std::log(1 + 1 / x) * std::pow(x, 2) + 0.5 * x - 0.5 * std::log(x + 1);
				};
				Float up = indef(high_temp + 1) - indef(low_temp);
				Float low = indef(high_temp) - indef(low_temp);
				mean = norm_const * (up + low) / 2;
			}
			else if (p == 2) { // case of summing n/n^2 = 1/n
				Float up = 1 / (low_temp + 1) + std::log((high_temp + 1) / (low_temp + 1));
				Float low = std::log((high_temp + 2) / (low_temp + 1));
				Float sum = (up + low) / 2;
				mean = norm_const * 1 / (1 - p) *
					(std::pow(high_temp + 1, 2 - p) - std::pow(low_temp, 2 - p) - sum);
			}
			else { // other cases, but still want p >=0 (this could be relaxed, though)
				if (p > 1) { // sequence n*(cdf(n+1)-cdf(n)) is decreasing
					Float low = 1 / (2 - p) * (std::pow(high_temp + 2, 2 - p) - std::pow(low_temp + 1, 2 - p));
					Float up = std::pow(low_temp + 1, 1 - p) +
						1 / (2 - p) * (std::pow(high_temp + 1, 2 - p) - std::pow(low_temp + 1, 2 - p));
					Float sum = (up + low) / 2;
					mean = norm_const * 1 / (1 - p) *
						(std::pow(high_temp + 1, 2 - p) - std::pow(low_temp, 2 - p) - sum);
				}
				else if (p > 0) { // sequence n*(cdf(n+1)-cdf(n)) is increasing
					Float low = 1 / (2 - p) * (std::pow(high_temp + 1, 2 - p) - std::pow(low_temp, 2 - p));
					Float up = 1 / (2 - p) * (std::pow(high_temp + 2, 2 - p) - std::pow(low_temp + 1, 2 - p));
					Float sum = (up + low) / 2;
					mean = norm_const * 1 / (1 - p) *
						(std::pow(high_temp + 1, 2 - p) - std::pow(low_temp, 2 - p) - sum);
				}
				else if (p==0) { // then p is 0
					mean = norm_const * (high_temp - low_temp + 1) * ((high_temp + low_temp) / 2);
				}
				else{ // all other p
					Float low = std::pow(low_temp+1, 1-p) + ( std::pow(high_temp+1,2-p) - std::pow(low_temp+1,2-p) )/(2-p);
					Float up = ( std::pow(high_temp+2,2-p) - std::pow(low_temp+1,2-p)  )/(2-p);
					Float sum = (up+low)/2;
					mean = norm_const / (1-p) * ( std::pow(high_temp+1,2-p) - std::pow(low_temp,2-p) - sum);
				}
			}

			if (low == 0) { // shift backwards
				mean -= 1;
			}
		}

	public:

		/**
		Constructor to set the p-power
		@param _p the power such that pmf ~ 1/n^p
		*/
		power_pmf(Float _p = 3, Int _low = 0,
			Int _high = std::numeric_limits<Int>::max() / 2) : p(_p), low(_low), high(_high), mean(0) {

			set_mean();
		}

		/**
		call will return an Int from distribution ~1/n
		@return Int from the distribution
		*/
		Int operator()() const override {

			// here we find x s.t.
			// int_low^x 1/y^p dy = u and
			// return the integer part

			Float low_temp = low, high_temp = high;

			if (low == 0) { // will need to shift back afterwards
				low_temp = 1;
				high_temp = static_cast<Float>(high) + 1;
			}

			static thread_local std::uniform_real_distribution<Float> u_rand;

			Float u = u_rand(integer_distribution<Int, Float>::get_eng()); // from [0, 1)

			Float temp = 0;
			// will be on [low, high+1)

			if (p != 1) { // no logs
				temp = std::pow(std::pow(low_temp, 1 - p) - u * (std::pow(low_temp, 1 - p)
					- std::pow(high + 1, 1 - p)), 1. / (1 - p));
			}
			else { // log case
				temp = low_temp * std::exp(u * std::log((high_temp + 1) / low_temp));
			}

			if (temp < 1) { // just in case of weird roundoff stuff
				temp = 1;
			}

			if (low == 0) { // shift down so 0 is lower bound
				return static_cast<Int>(temp) - 1;
			}
			else { // find to cast normally
				return static_cast<Int>(temp);
			}

		}


		/**
		gives the mean of the distribution
		@return the mean, could be infinite if p <= 2
		*/
		Float get_mean() const override {
			return mean;
		}

		/**
		returns the parameters
		@return a tuple of the power, low and high values, and the mean
		*/
		auto get_params() const {
			return std::tie(p, low, high, mean);
		}

		/**
		determines if this distribution equals another
		@param other the distribution to compare against
		@return if they are equal in both type of distribution (power_pmf) and parameters
		*/
		bool equal(const integer_distribution<Int, Float>& other) const override {
			// must be the same dynamic type and have same parameters
			if (auto p = dynamic_cast<const power_pmf*>(&other)) {
				return get_params() == p->get_params();
			}
			return false;
		}
	};


	/**
	@class fixed_value represents a "distribution" that will only ever return a single value
	@tparam Int an unsigned integer type to return
	@tparam Float a level of precision (unused in this case besides for inheritance)
	*/
	template<typename Int, typename Float>
	class fixed_value : public integer_distribution<Int, Float> {

	private:
		Int val; // the value always being returned

	public:

		/**
		Constructor to set the value to return
		@return the fixed value
		*/
		fixed_value(Int _val) : val(_val) {}

		/**
		Call always gives the fixec value
		@return the fixed value
		*/
		Int operator()() const override {
			return val;
		}

		/**
		finds the mean of the single value - not very interesting
		@return the mean, the fixed value
		*/
		Float get_mean() const override {
			return static_cast<Float>(val);
		}

		/**
		returns the parameters
		@return the value
		*/
		const auto& get_params() const {
			return val;
		}

		/**
		determines if this distribution equals another
		@param other the distribution to compare against
		@return if they are equal in both type of distribution (fixed_value) and parameters
		*/
		bool equal(const integer_distribution<Int, Float>& other) const override {
			// must be of same type and have same value
			if (auto p = dynamic_cast<const fixed_value*>(&other)) {
				return get_params() == p->get_params();
			}
			return false;
		}
	};



	/**
	@class initial_infected is an auxiliary class to help
	initializing sick members in the population
	*/
	class initial_infected {
	public:
		/**
		destructor: pure virtual as this class should be abstract
		*/
		virtual ~initial_infected() = 0;
	};




	/**
	@class fraction_infected signifies the initial asymptomatic
	people are chosen based on a fraction of the total population
	*/
	class fraction_infected : public initial_infected {
	private:
		double fraction; // fraction of population to make asymptomatic
	public:
		/**
		initializes the class with the fraction to infect
		@param _frac the fraction of the population who will be asymptomatic
		*/
		fraction_infected(double _frac);

		/**
		conversion to double
		@return the fraction to infect
		*/
		operator double() const;
	};

	/**
	@class numbers_infected is used to initialize specific numbers of people
	within the population to be asymptomatic or ill
	*/
	class numbers_infected : public initial_infected {
	private:
		std::vector<size_t> numbers_asymptomatic; // asymptomatic numbers, group by group
		std::vector<size_t> numbers_ill; // ill numbers, group by group
	public:
		/**
		constructor to sets the counts
		@param nums_asymptomatic number of asymptomatic people: general, disabled, caregiver, essential
		@param nums_ill number of ill people: general, disabled, caregiver, essential
		*/
		numbers_infected(std::vector<size_t> nums_asymptomatic,
			std::vector<size_t> nums_ill);

		/**
		gives a vector of asymptomatic counts in respective order: general, disabled, caregiver, essential
		*/
		const std::vector<size_t>& get_vec_asymptomatic() const;

		/**
		gives the vector of symptomatic counts in respective order: general, disabled, caregiver, essential
		*/
		const std::vector<size_t>& get_vec_ill() const;
	};



	/**
	@class graph represents the entire population of people
	in various states with their connections
	*/
	class graph {
	public:

		// some useful aliases: the shared_ptr allows for dynamic casts and 
		// prevents memory leaks

		// how initial infecteds are specified
		using initial_infected_type = std::shared_ptr<initial_infected>;

		// how distributions are specified
		using dist_type = std::shared_ptr<integer_distribution<size_t, double>>;

		// a power law
		using power_type = power_pmf<size_t, double>;
		// a discrete (empirical) distribution
		using discrete_type = discrete_distribution<size_t, double>;
		// a fixed distribution
		using fixed_type = fixed_value<size_t, double>;

		/**
		@struct graph_structure stores all the parameters
		relevant for the graph simulation
		*/
		struct graph_structure {
			/* population level data */
			initial_infected_type initial_infected_values; // how many initially infected

			size_t total_population; // population of city

			double general_percent; // how many in general population
			double client_percent; // how many disabled
			double caregiver_percent; // how many caregivers
			double essential_worker_percent; // percent of people essential

			/* contact data */
			dist_type general_strong_dist; // how many close connections in general
			dist_type general_weak_dist; // how many loose connections/friends/coworkers in general

			dist_type caregiver_strong_dist; // how many close connections for caregivers
			dist_type caregiver_weak_dist; // how many loose connections/friends/coworkers for caregivers

			dist_type client_strong_dist; // how many close connections for clients
			dist_type client_weak_dist; // how many loose connections/friends/coworkers for clients

			dist_type essential_strong_dist; // strong contacts essential workers
			dist_type essential_weak_dist; // weak contacts essential workers

			dist_type weak_caregivers_per_client_dist; // how many weak caregivers a client sees
			dist_type strong_caregivers_per_client_dist; // how many strong caregivers a client sees

			size_t daily_caregivers; // how many caregivers seen per day

			/* PPE and behaviour stuff */

			double pr_break_weak; // probability caregiver will not break weak if ill
			double ppe_reduction; // reduced transmission chance with PPE
			double weak_edge_weight; // weight of a weak contact
			double strong_edge_weight; // weight of a strong contact
			double caregiver_client_weight; // weight of caregiver-client

			bool caregiver_wears_ppe; // if the caregivers wear PPE
			bool client_wears_ppe; // if the clients wear PPE
			bool essentials_wear_ppe; // if essential workplaces have PPE
			bool all_weak_ppe; // if all weak contacts have PPE

			/* model rate stuff */
			double beta_S_to_E; // Exponential rate S + I -> E and S + A -> E combinations
			double nu_E_to_A; // Exponential rate E -> A 
			double alpha_A_to_I; // Exponential rate A -> I
			double mu_I_to_H; // Exponential rate I -> H
			double zeta_H_to_R; // Exponential rate H -> R
			double rho_I_to_R; // Exponential rate I -> R
			double eta_A_to_R; // Exponential rate A -> R
			double gamma_R_to_S; // Exponential rate R -> S again!

			double pr_tested_if_ill; // probability of being tested if ill			

			/* strategic stuff */
			struct changes {
				double strat_t; // when distancing begins
				dist_type strat_general_weak_dist; // how many loose connections/friends/coworkers in general
				dist_type strat_caregiver_weak_dist; // how many loose connections/friends/coworkers for caregivers
				dist_type strat_client_weak_dist; // how many loose connections/friends/coworkers for clients
				dist_type strat_essential_weak_dist; // weak contacts essential workers
				bool strat_caregiver_wears_ppe; // if the caregivers wear PPE
				bool strat_client_wears_ppe; // if the clients wear PPE
				bool strat_essentials_wear_ppe; // if essential workplaces have PPE
				bool strat_all_weak_ppe; // if all weak wear PPE
			} close, open;

		};

		/**
		@class graph_structure_reader is used to process a text file storing parameter inputs,
		storing a string and offering conversions to the different types of parameters we need
		*/
		class graph_structure_reader {
		private:
			std::string content;
		public:
			/**
			constructor to store the content
			@param _content what will be stored for later conversion
			*/
			graph_structure_reader(std::string _content) : content(std::move(_content)) {}

			/**
			conversion to initial infected object pointer
			@return either points to numbers or fraction
			*/
			explicit operator initial_infected_type() const {
				std::istringstream iss(content);
				std::string choice;
				iss >> choice; // whether 'fraction' or 'numbers'
				if (choice == "fraction") { // they wanted fraction
					double val;
					iss >> val;
					return std::make_shared<fraction_infected>(val);
				}
				else if (choice == "numbers") { // they have a list of numbers for 'numbers'
					const size_t num_groups = 4;
					std::vector<size_t> nums_asymptomatic, nums_ill;
					size_t counted = 0;
					size_t num;
					while (counted++ < num_groups) { // read in one per group -- 4 groups
						iss >> num;
						if (!iss) { // failed to read
							throw std::runtime_error("Failed on reading asymptomatic");
						}
						nums_asymptomatic.push_back(num);
					}
					counted = 0;
					while (counted++ < num_groups) { // read in one per group -- 4 groups
						iss >> num;
						if (!iss) {
							throw std::runtime_error("Failed on reading ill");
						}
						nums_ill.push_back(num);
					}
					return std::make_shared<numbers_infected>(nums_asymptomatic, nums_ill);
				}
				else { // bad case
					throw std::runtime_error("invalid format for initial infectives");
				}

			}

			/**
			to convert to a floating point type
			@tparam Float the floating to convert to
			@return the content as that Float type
			*/
			template<typename Float, std::enable_if_t< std::is_floating_point_v<Float>, void* > = nullptr >
			explicit operator Float() const {
				std::istringstream iss(content);
				Float f{};
				iss >> f;
				return f;
			}

			/**
			to convert to an unsigned integer type
			@tparam Int the unsigned integer type to convert to
			@return the content as that unsgined type
			*/
			template<typename Int, std::enable_if_t< std::is_unsigned_v<Int>, void* > = nullptr >
			explicit operator Int() const {
				std::istringstream iss(content);
				Int i{};
				iss >> i;
				return i;
			}

			/**
			to convert to an unsigned integer type
			@tparam Int the unsigned integer type to convert to
			@return the content as that unsgined type
			*/
			explicit operator bool() const {

				// need to search for the "true" or "false" string

				if (content.find("true") != std::string::npos) { // found true
					return true;
				}
				else if (content.find("false") != std::string::npos) { // found false
					return false;
				}
				else { // found neither - not good!
					throw std::runtime_error("Cannot convert to bool here");
				}
			}

			/**
			to convert to an integer distribution
			@tparam Int the integer type it returns
			@tparam Float the floating point of its precision
			@return the integer distribution
			*/
			template<typename Int, typename Float>
			explicit operator std::shared_ptr<integer_distribution<Int, Float>>() const {
				std::istringstream iss(content);

				// option chosen
				std::string opt;
				iss >> opt;

				if (!iss) { // option not parsed somehow
					std::cerr << "distribution reading stream failed" << std::endl;
					throw std::runtime_error("stream has failed!");
				}

				if (opt == "power") { // power so specify p and bounds
					Float p = 0;
					Int low = 0;
					Int high = 0;
					iss >> p >> low >> high;
					if (!iss) { // if fail
						std::cerr << "failed to read power" << std::endl;
						throw std::runtime_error("failed to read power");
					}
					return std::make_shared<power_type>(p, low, high);
				}
				else if (opt == "empirical") { // given the 0, 1, 2, etc. counts
					std::vector<Float> vec;
					Float f;
					while (iss >> f) { // until stream fails, push_back
						vec.push_back(f);
					}
					return std::make_shared<discrete_type>(std::move(vec));
				}
				else if (opt == "fixed") { // just a single value
					Float val;
					iss >> val;
					if (!iss) { // if fail
						std::cerr << "failed to read fixed" << std::endl;
						throw std::runtime_error("failed to read fixed");
					}
					return std::make_shared<fixed_type>(val);
				}
				else { // selection was not made...
					std::cerr << "need to specify distribution type: power/empirical/fixed" << std::endl;
					throw std::runtime_error("need to specify distribution type: power/empirical/fixed");
				}

			}

			/**
			to convert to a discrete distribution
			@tparam Int the integer type it returns
			@tparam Float the floating point of its precision
			@return the discrete distribution
			*/
			template<typename Int, typename Float,
				typename = std::enable_if_t< std::is_unsigned_v<Int>&& std::is_floating_point_v<Float> > >
				explicit operator discrete_distribution<Int, Float>() const {
				std::istringstream iss(content);
				std::vector<Float> vec;
				Float f;
				while (iss >> f) { // until stream fails, push_back
					vec.push_back(f);
				}
				return { std::move(vec) };
			}

			/**
			to convert to a power_pmf distribution
			@tparam Int the integer type it returns
			@tparam Float the floating point of its precision
			@return the power_pmf distribution
			*/
			template<typename Int, typename Float,
				typename = std::enable_if_t< std::is_unsigned_v<Int>&& std::is_floating_point_v<Float> > >
				explicit operator power_pmf<Int, Float>() const {
				std::istringstream iss(content);
				Float f;
				iss >> f;
				return power_pmf<Int, Float>{f};
			}
		};

	private:
		/**
		@class node represents a person in various disease states with their contacts
		*/
		class node;

		using person = node;

		/**
		@class caregiver represents a caregiver, more specialized than a node
		*/
		class caregiver;

		/**
		@class client represents a disabled person, more specailized than a node
		*/
		class client;

		/**
		@class essential_worker is an essential worker
		*/
		class essential_worker;

		/**
		@class family_unit represents a family (of strong contacts)
		*/
		class family_unit;

		/**
		@class edge represents a connection between two people
		*/
		class edge {
			// graph needs access to some private member functions
			friend graph;

		private:
			node* contact; // pointed to
			double weight; // strength
			bool active; // whether active
		public:

			/**
			default constructor for edge
			makes the contact null, the weight 0, and the activity false
			*/
			edge();

			/**
			constructs an edge
			@param _contact the connection
			@param _weight the edge weight, 1 by default
			@param _active, whether the connection is active
			*/
			edge(node* _contact, double _weight = 1, bool _active = true);

			/**
			states whether a node connection is active
			@return true if it is, false otherwise
			*/
			bool is_active() const;

			/**
			updates the activity status
			@param _active the new activity
			*/
			void set_active_status(bool _active);

			/**
			updates the weight of the edge
			@param _weight the new weight
			*/
			void set_weight(double _weight);

			/**
			determines if node is the one being pointed to
			@param other the other node
			*/
			bool points_to(const node* other) const;

			/**
			specifies node being pointed to
			@return the node pointed to
			*/
			const node* points_to() const;

			/**
			specifies node being pointed to - nonconst version
			@return the node pointed to
			*/
			node* points_to();
		};

		using sub_population = std::vector< std::unique_ptr<node> >; // subpopulation stores each person
		using population = std::map<std::string, sub_population>; // population has names of each subpopulation
		using cumulatives = std::map<std::string, size_t>; // cumulative counts


		// the labels we'll use
		static constexpr const char* gen = "General";
		static constexpr const char* care = "Caregivers";
		static constexpr const char* dis = "Disabled";
		static constexpr const char* ess = "Essential";

		// the entire population
		population people;

		std::pair<size_t, sub_population*> gen_ptr, care_ptr, dis_ptr, risk_ptr, ess_ptr;

		/**
		@class cumulative_outcome represents an outcome of cases to document
		*/
		enum class cumulative_outcome {
			I_inc,
			H_inc,
			R_inc,
			other
		};

		// counts for each category
		cumulatives i_cumulatives{ {gen,0},{care,0}, {dis,0}, {ess,0} };
		cumulatives h_cumulatives{ {gen,0},{care,0}, {dis,0}, {ess,0} };
		cumulatives r_cumulatives{ {gen,0},{care,0}, {dis,0}, {ess,0} };

		// positive tests
		cumulatives pos_cumulatives{ {gen,0},{care,0}, {dis,0}, {ess,0} };

		graph_structure settings; // properties of the graph

		thread_local static std::random_device rd; // for randomness
		thread_local static std::default_random_engine eng; // for randomness

		// to safely print
		static inline std::mutex printer_mutex;

		/**
		prints all the stats of people in each state and group
		@param out the stream to print to
		*/
		void print_stats(std::ostream& out) const;

		/**
		provides tallies of how many people in each group
		are in each state
		@return disease state counts per group
		*/
		group_states do_counts() const;

		/**
		does the intensity calculations for all people, updating their state
		*/
		void calculate_intensities();

		/**
		steps all people forward by a given time step
		@param dT the time step
		*/
		void step_all_forward(double t, double dT);

		/**
		prints relevant social statistics
		*/
		void print_demographic_statistics() const;

		/**
		makes a vector of the weak ids for people
		to be chosesn from
		@return vector of each person's id with multiplicity
		based on their number weak contacts
		*/
		std::vector<size_t> generate_weak_ids() const;

		/**
		breaks some contacts up and returns new weak ids to pair
		@return vector of each person's id with multiplicity based
		on their number of weak contacts to have

		@param oc whether we are opening or closing up
		*/
		void redistribute_weak(Open_Close oc);

		/**
		makes a vector of the strong ids for people
		to be chosesn from
		@return vector of vectors where each vector enclosed
		stores the grouping of ids for a family unit
		*/
		std::vector< std::vector<size_t> > generate_strong_ids() const;

		/**
		sets all target weak and strong counts
		*/
		void set_weak_strong_target_counts();

		/**
		sets all the weak counts
		*/
		void set_weak_target_counts();

		/**
		sets all the strong counts
		*/
		void set_strong_target_counts();

		/**
		returns a person from the entire population
		based on their index within it
		Remark: implicitly, the order is general, caregivers, clients,
		and at_risk_caregivers
		@param i the index in the population
		@return a reference to the unique_ptr representing the person
		*/
		std::unique_ptr<node>& person_finder(size_t i);

		/**
		returns a person from the entire population
		based on their index within it - const version
		Remark: implicitly, the order is general, caregivers, clients,
		and at_risk_caregivers
		@param i the index in the population
		@return a reference to the unique_ptr representing the person
		*/
		const std::unique_ptr<node>& person_finder(size_t i) const;

		/**
		returns a caregiver from the population
		of caregivers and at_risk_caregivers
		Remark: implicitly, the order is caregivers
		and at_risk_caregivers
		@param i the index in the population
		@return a reference to the unique_ptr representing the caregiver
		*/
		std::unique_ptr<node>& caregiver_finder(size_t i);

		/**
		Does the assignment of weak contacts
		@param weak_ids the ids to use for pairing of people
		*/
		void assign_weak(std::vector<size_t> weak_ids);

		/**
		Does the assignment of weak contacts
		@param strong_ids the ids to use for pairing of people
		*/
		void assign_strong(std::vector< std::vector<size_t> > strong_ids);

		/**
		Does the assignment of caregivers and clients
		*/
		void pair_clients_and_caregivers();

		/**
		will randomly make some people ill, used if given a fraction
		*/
		void make_some_asymptomatic_or_ill();

		/**
		will make a certain number of general, client, caregiver, at risk normal,
		at risk caregiver, and essential
		people ill in respective order
		@param numbers the numbers descirbed above
		*/
		void make_numbers_ill(const std::vector<size_t>& numbers);

		/**
		will make a certain number of general, client, caregiver, at risk normal,
		at risk caregiver, and essential
		people asymptomatic in respective order
		@param numbers the numbers descirbed above
		*/
		void make_numbers_asymptomatic(const std::vector<size_t>& numbers);

		/**
		will make population asymptomatic at random
		@param fract the fraction to make ill
		*/
		void make_fraction_asymptomatic(double frac);

		/**
		creates general people, clients, and caregivers
		*/
		void initialize_population();


		/**
		deploys social distance strategy

		@param opt the type of changes being made -- a changes variable
		@param oc whether opening or closing
		*/
		void deploy_strategy(const graph_structure::changes& opt, Open_Close oc);

		/**
		chooses a random set of caregivers to take care of each disabled that day
		*/
		void assign_daily_caregivers();


	public:
		/**
		constructs a graph given the appropriate settings, number of contacts, etc.
		@param _settings the graph properties
		*/
		graph(const graph_structure& _settings);

		/**
		initializes the population based on the settings
		*/
		void initialize();

		/**
		runs the simulations forward a give number of timesteps
		@param dT the time interval
		@param num_iter the number of iterations to run
		@param out a stream to write results to, std::cout by default
		*/
		void run(double dT, int num_iter, std::ostream& out = std::cout);


	};


	class graph::node {
	private:
		friend void graph::make_numbers_ill(const std::vector<size_t>& nums);

		static thread_local std::random_device rd;
		static thread_local std::default_random_engine eng;

		static thread_local inline size_t next_id = 0;
		size_t id;

		const graph* g; // the graph it belongs to		
		state status; // the disease state S, E, A, I, H, or R

		size_t target_strong_size; // how many strong contacts to have, ideally
		size_t target_weak_size; // how many weak contacts to have, ideally

		std::map<size_t, edge> strong_contacts; // close connections
		std::map<size_t, edge> weak_contacts; // friends, coworkers, etc.
		double pr_not; // probability of not getting COVID 
		bool will_break_weak; // whether will break weak if ill
		bool tested_if_ill; // if person will get tested when ill
		bool confirmed_positive; // if they are confirmed positive

		std::shared_ptr<const family_unit> f; // family unit they belong to

		/**
		if someone goes to the hosptial
		*/
		void go_to_hospital();

	protected:

		/**
		returns the pointer to the graph
		*/
		const graph* get_graph() const {
			return g;
		}

		/**
		breaks the strong contacts, when go to hospital
		*/
		virtual void break_strong();

		/**
		breaks the weak contacts when display illness
		*/
		virtual void break_weak();


		/**
		opens up contacts again post-recovery
		*/
		virtual void reset_contacts();

		/**
		to further scale intensity due to clients/caregivers
		@param _factor the amount to scale by
		*/
		void scale_not(double _factor);

		/**
		provides the settings
		@return the graph_structure
		*/
		const graph_structure& get_structure() const;

		/**
		removes a contact
		@other the node to remove as strong contact
		@last_step if the other person has already done the removing
		*/
		void remove_strong_contact(node& other, bool last_step);


	public:

		/**
		node constructor starts person off as healthy with 0 intensity
		@param _g the graph it belongs to
		@param _pr_break_weak probability of breaking
		weak contacts if sick
		*/
		node(const graph* _g, double _pr_break_weak);

		/**
		destructor
		*/
		virtual ~node() = default;

		/**
		move constructor
		*/
		node(node&&) noexcept = default;

		/**
		copy constructor
		*/
		node(const node&) = default;

		/**
		move assignment
		*/
		node& operator=(node&&) noexcept = default;

		/**
		copy assignment
		*/
		node& operator=(const node&) = default;

		/**
		determintes if two people are active contacts
		*/
		virtual bool is_contact(const node& other) const;

		/**
		based on current contacts, calculates the probability of not getting infected
		but derived classes need a bit more work
		*/
		virtual void calculate_pr_not();

		/**
		takes one step forward of a given length
		@param dT the time step
		@return if I, H, or R increases
		*/
		cumulative_outcome evolve(double dT);

		/**
		adds someone as a strong contact and does so in a reciprocated way
		@param other the other person
		@param _weight the edge weight
		*/
		void add_strong_contact(node& other, double _weight = 1);


		/**
		adds someone as a weak contact and does so in a reciprocated way
		@param other the other person
		@param _weight the edge weight
		*/
		void add_weak_contact(node& other, double _weight = 1);

		/**
		clears all weak contacts
		*/
		void clear_weak();

		/**
		gives the current disease status
		@return the state
		*/
		state get_status() const;

		/**
		turns off the edge with other
		@param other the other node
		*/
		void turn_off_weak(const node* other);

		/**
		turns off the edge with other
		@param other the other node
		*/
		void turn_off_strong(const node* other);

		/**
		turns on the edge with other
		@param other the other node
		*/
		void turn_on_weak(const node* other);

		/**
		turns on the edge with other
		@param other the other node
		*/
		void turn_on_strong(const node* other);

		/**
		whether a person is acting normal
		@return if in S, E, A, or R
		*/
		bool no_symptoms() const;

		/**
		whether a person is contagious
		@return if in A or I or H
		*/
		bool is_contagious() const;

		/**
		makes person ill in a state
		@param disease_state the disease state
		*/
		void make_ill(state disease_state);

		/**
		whether they will break weak contacts with illness
		@return the will_break_weak value
		*/
		bool breaks_weak() const;

		/**
		assigns person to a family unit
		@param fam the family unit being assigned to
		*/
		void assign_family_unit(std::shared_ptr<family_unit> fam);

		/**
		if the person has been confirmed positive
		@return their status
		*/
		bool been_confirmed_positive() const;

		/**
		the target strong contact size
		@return the target value of the family size: in reality the family size might be smaller
		*/
		size_t get_target_strong_size() const;

		/**
		the target weak contact size
		@return the target value of the family size: in reality the family size might be smaller
		*/
		size_t get_target_weak_size() const;

		/**
		how many weak contacts they currently have
		@return size size of weak contacts
		*/
		size_t get_weak_contact_count() const;

		/**
		how many strong contacts they currently have
		@return size of strong contacts
		*/
		size_t get_strong_contact_count() const;

		/**
		sets the target strong contact size
		@param _new_size the new target size
		*/
		void assign_target_strong_size(size_t _new_size);

		/**
		sets the new target weak contact size
		@param _new_size the new target size
		*/
		void assign_target_weak_size(size_t _new_size);

		/**
		clears excessive weak contacts
		*/
		void clear_excess_weak();

		/**
		removes a given contact
		@param other the contact to remove
		*/
		void remove_weak_contact(node* other);

		/**
		returns the person's id
		@return their unique id
		*/
		size_t get_id() const;

		/**
		returns a disabled contact, if possible [this function is no longer used]
		@return a disabled family member, if this can be done
		*/
		std::optional<const client*> get_disabled_family() const;
	};



	class graph::essential_worker : public graph::person {
	public:
		/**
		constructor makes an essential worker
		@param _g the graph it belongs to
		@param _pr_break_weak the probability of breaking
		weak contact if ill
		*/
		essential_worker(const graph* _g, double _pr_break_weak);

	};




	class graph::caregiver : public graph::node {
		friend client; // client needs to access private functions

	private:
		std::map < size_t, edge > clients; // the clients	
		bool wear_ppe_w_client;

		/**
		turns off the edge with other client
		@param other the client
		*/
		void turn_off_client(const node* other);

		/**
		turns on the edge with a client
		@param other the client
		*/
		void turn_on_client(const node* other);

	protected:

		/**
		determines if someone is a client
		@param other the possible client
		@return if they are a client
		*/
		bool is_client(const client& other) const;

		/**
		calculates the probability of not getting sick, taking into account client interactions
		*/
		void calculate_pr_not() override;

		/**
		resets the clients
		*/
		void reset_clients();

		/**
		makes contacts go back to normal after recovery if possible,
		including clients
		*/
		void reset_contacts() override;

		/**
		break the client connections
		*/
		void break_clients();

		/**
		calculates extra factor due to caregiver duties
		@return the factor
		*/
		double extra_caregiver_factor() const;

		/**
		breaks the weak contacts
		*/
		void break_weak() override;

	public:
		/**
		constructor makes a caregiver
		@param _g the graph it belongs to
		@param _pr_break_weak the probability of breaking
		@param _wears_ppe if they wear PPE with their clients
		weak contact if ill
		*/
		caregiver(const graph* _g, double _pr_break_weak, bool _wears_ppe);

		/**
		determines if another node is a contact, includes client possibility
		@param other the other node
		@return if a contact
		*/
		bool is_contact(const node& other) const override;


		/**
		adds person as client
		@param c the client to add
		@param _weight the edge weight
		*/
		void add_client(client& c, double _weight = 1.0);

		/**
		breaks strong contacts for a caregiver
		*/
		void break_strong() override;

		/**
		if ppe is worn when with client
		@return if wearing PPE when with their client
		*/
		bool ppe_worn_with_client() const;

		/**
		how many clients they have
		@return number of clients
		*/
		size_t num_clients() const;

		/**
		sets if ppe is worn
		@param wears_ppe new ppe property
		*/
		void set_ppe(bool wears_ppe);

	};


	class graph::client : public graph::node {
		friend caregiver;
	private:
		// caregiveres that they stop seeing if they show illness
		std::map < size_t, edge > weak_caregivers;

		// caregivers they still see even if they are ill
		std::map < size_t, edge > strong_caregivers;

		// the caregivers they have on a given day
		std::map< size_t, edge > daily;

		bool wear_ppe_w_caregiver;

		/**
		breaks weak connections, including with caregivers
		*/
		void break_weak() override;

		/**
		breaks the strong connections, now including the
		strong caregivers
		*/
		void break_strong() override;

		/**
		for when recovery has taken place, can also get back caregivers if possible
		*/
		void reset_contacts() override;

		/**
		turns off the edge with other caregiver
		@param other the caregiver
		*/
		void turn_off_caregiver(const node* other);

		/**
		turns on the edge with another caregiver
		@param other the other caregiver
		*/
		void turn_on_caregiver(const node* other);

		/**
		calculates the probability of not getting sick for the client taking
		into account interactions with caregivers
		*/
		void calculate_pr_not() override;
	protected:

		/**
		determines if someone is a caregiver
		@param other the possible caregier
		@return if they are a ccaregiver
		*/
		bool is_caregiver(const caregiver& other) const;

		/**
		reactivates the caregivers
		*/
		void reset_caregivers();

		/**
		breaks the weak connections with caregivers
		*/
		void break_weak_caregivers();

		/**
		breaks the stron connections with caregivers
		*/
		void break_strong_caregivers();

		/**
		calculates extra factor due to being a client
		@return the factor
		*/
		double extra_client_factor() const;

	public:
		/**
		constructs a client
		@param _g the graph it belongs to
		@param _pr_break_weak the probability of breaking weak
		@param _wears_pee if they wear PPE with their caregivers
		contacts if ill
		*/
		client(const graph* _g, double _pr_break_weak, bool _wears_ppe);

		/**
		determines if another person is a contact, possibly a caregiver
		@param other the other node
		*/
		bool is_contact(const node& other) const override;

		/**
		adds person as a weak caregiver
		@param c the caregiver to add
		@param _weight the edge weight
		*/
		void add_weak_caregiver(caregiver& c, double _weight);

		/**
		adds person as a strong garegiver
		@param c the caregiver to add
		@param _weight the edge weight
		*/
		void add_strong_caregiver(caregiver& c, double _weight);

		/**
		if ppe is worn when with caregiver
		@return if wearing PPE when with their caregiver
		*/
		bool ppe_worn_with_caregiver() const;

		/**
		selects caregivers for the day
		*/
		void assign_chosen_caregivers();

		/**
		determines if a caregiver is providing care at present
		@return if they are
		*/
		bool is_receiving_care_from(const caregiver* c) const;

		/**
		how many strong caregivers they have
		@return the count
		*/
		size_t get_strong_caregiver_count() const;

		/**
		how many weak caregivers they have
		@return the count
		*/
		size_t get_weak_caregiver_count() const;

		/**
		sets if ppe is worn
		@param wears_ppe new ppe property
		*/
		void set_ppe(bool wears_ppe);

	};

	// definition of the class 
	class graph::family_unit {
		friend node; // make node a friend

	private:
		thread_local static int next_id;
		int id;
	public:
		/**
		default constructor makes a new, empty, family unit
		*/
		family_unit();

		/**
		gives the family's id
		@return family id
		*/
		int get_id() const;
	};


	/**
	returns all the settings based on an input file with
	[struct parameter name]: [value] [#COMMENT]
	@param file_name the name of the file
	@return all the settings
	*/
	graph::graph_structure read_parameters(std::string file_name);

	/**
	runs the simulations and writes the results to a file
	@param file_base the base part of the file for "[file_base][number].txt"
	@param file_id_low the lower id of the file
	@param file_id_low the upper id of the file
	@param settings the settings for the simulations
	@param dT the time step in days
	@param num_iter the number of iterations to run
	*/
	void run_simulations(const std::string& file_base, int file_id_low, int file_id_high, const graph::graph_structure& settings,
		double dT, int num_iter);

}

#endif 