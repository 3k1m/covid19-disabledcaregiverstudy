/**
@file CovidGraph.cpp
@brief provides implementations for many methods/functions of CovidGraph.h to simulate network model
@author Michael Lindstom, corresponding author: m i k e l [at] m a t h [dot] u c l a [dot] e d u
@author Thomas Valles
@date Sept 2021
@version 3.0

@note C++17 is required

@details see CovidGraph.h
*/

#include "CovidGraph.h"
#include<algorithm>
#include<map>
#include<fstream>
#include<string>
#include<sstream>
#include<iostream>
#include<stdexcept>

namespace covid {

	/*
	* Definitions for the Graph, nodes, caregivers, clients etc.
	*
	*/

	thread_local std::random_device graph::rd;
	thread_local std::default_random_engine graph::eng(graph::rd());

	thread_local std::random_device graph::node::rd;
	thread_local std::default_random_engine graph::node::eng(graph::node::rd());

	graph::graph(const graph::graph_structure& _settings) :
		settings(_settings) {}

	void graph::initialize() {

		// makes the initial member variable vectors
		// to represent everyone
		initialize_population();

		gen_ptr = { people.at(gen).size(), &people.at(gen) };
		care_ptr = { people.at(care).size(), &people.at(care) };
		dis_ptr = { people.at(dis).size(), &people.at(dis) };
		ess_ptr = { people.at(ess).size(), &people.at(ess) };

		// set the targets
		set_weak_strong_target_counts();

		// make vectors to represent the indices
		// within the population of a person, with their
		// stubs, weak or strong
		auto weak_ids = generate_weak_ids();


		auto strong_ids = generate_strong_ids();

		// move because then move constructor for
		// vector is invoked in the functions, faster transfer
		assign_weak(std::move(weak_ids));

		assign_strong(std::move(strong_ids));

		// next move on to clients and their caregivers
		// clients "pick" caregivers
		pair_clients_and_caregivers();

		make_some_asymptomatic_or_ill();		

	}

	void graph::calculate_intensities() {

		for (auto& pop : people) { // over ever subpopulation
			for (auto& p : pop.second) { // every person in that group
				p->calculate_pr_not();
			}
		}

	}


	void graph::step_all_forward(double t, double dT) {

		for (auto& p : pos_cumulatives) { // zero out all the values
			p.second = 0;
		}

		for (auto& pop : people) { // over ever subpopulation

			for (auto& p : pop.second) { // every person in that group
				auto res = p->evolve(dT);

				if (p->been_confirmed_positive()) { // increase if positive test
					++pos_cumulatives[pop.first];
				}

				if (res == cumulative_outcome::I_inc) { // someone gets ill
					++i_cumulatives[pop.first];
				}

				else if (res == cumulative_outcome::H_inc) { // goes to hospital
					++h_cumulatives[pop.first];
				}

				else if (res == cumulative_outcome::R_inc) { // they recovered
					++r_cumulatives[pop.first];
				}

			}
		}
	}

	void graph::run(double dT, int num_iter, std::ostream& out) {

		double time = 0;

		
		out << "Time\tIteration" <<
			"\tGen S\tGen E\tGen A\tGen I\tGen H\tGen R" <<
			"\tCare S\tCare E\tCare A\tCare I\tCare H\tCare R" <<
			"\tDis S\tDis E\tDis A\tDis I\tDis H\tDis R" <<
			"\tEss S\tEss E\tEss A\tEss I\tEss H\tEss R" <<
			"\tGenTtl I\tGenTtl H\tGenTtl R\tGenTtl +" <<
			"\tCareTtl I\tCareTtl H\tGenTtl R\tCareTtl +" <<
			"\tDisTtl I\tDisTtl H\tDisTtl R\tDisTtl +" <<
			"\tEssTtl I\tEssTtl H\tEssTtl R\tEssTtl +\n";

		bool has_closed = false;
		bool has_opened = false;

		for (int i = 0; i < num_iter; ++i) { // do this num_iter times			
			out << time << '\t' << i << '\t';
			print_stats(out);
			assign_daily_caregivers();
			calculate_intensities();
			step_all_forward(time, dT);
			time += dT;


			if (time < settings.close.strat_t) { // before first close
				// do nothing
			}
			else if (time < settings.open.strat_t) { // to get here, time >= close time
				if (!has_closed) { // have not closed yet
					deploy_strategy(settings.close, Open_Close::CLOSE);
					std::cout << "time: " << time << '\n';					
					has_closed = true; // now has closed
				}
			}
			else { // have reached opening time
				if (!has_opened) { // have not closed yet
					deploy_strategy(settings.open, Open_Close::OPEN);
					std::cout << "time: " << time << '\n';					
					has_opened = true; // now has closed
				}
			}

		}

		// one last printout
		out << time << '\t' << num_iter << '\t';
		print_stats(out);

	}

	void graph::print_stats(std::ostream& out) const {

		group_states now = do_counts();

		out << now.general.S << '\t' << now.general.E << '\t'
			<< now.general.A << '\t' <<
			now.general.I << '\t' << now.general.H << '\t' <<
			now.general.R << '\t';

		out << now.caregivers.S << '\t' << now.caregivers.E << '\t'
			<< now.caregivers.A << '\t' <<
			now.caregivers.I << '\t' << now.caregivers.H << '\t' <<
			now.caregivers.R << '\t';

		out << now.clients.S << '\t' << now.clients.E << '\t'
			<< now.clients.A << '\t' <<
			now.clients.I << '\t' << now.clients.H << '\t' <<
			now.clients.R << '\t';

		out << now.essential.S << '\t' << now.essential.E << '\t'
			<< now.essential.A << '\t' <<
			now.essential.I << '\t' << now.essential.H << '\t' <<
			now.essential.R;

		std::vector<std::string> keys{ gen, care, dis, ess };

		for (const auto& k : keys) { // each pop name
			out << '\t' << i_cumulatives.at(k) << '\t' <<
				h_cumulatives.at(k) << '\t' << r_cumulatives.at(k) << '\t' <<
				pos_cumulatives.at(k);
		}

		out << '\n';

	}

	group_states graph::do_counts() const {

		group_states res; // the result

		// counts for general first
		size_t S = 0, E = 0, A = 0, I = 0, H = 0, R = 0;

		for (const auto& p : people.at(gen)) { // look in general
			if (p->get_status() == state::S) { // state S
				++S;
			}
			else if (p->get_status() == state::E) { // state E
				++E;
			}
			else if (p->get_status() == state::A) { // state A
				++A;
			}
			else if (p->get_status() == state::I) { // state I
				++I;
			}
			else if (p->get_status() == state::H) { // state H
				++H;
			}
			else if (p->get_status() == state::R) { // state R
				++R;
			}
		}

		res.general = { S, E, A, I, H, R };

		// reset and do for clients

		S = 0, E = 0, A = 0, I = 0, H = 0, R = 0;

		for (const auto& p : people.at(dis)) { // look in clients
			if (p->get_status() == state::S) { // state S
				++S;
			}
			else if (p->get_status() == state::E) { // state E
				++E;
			}
			else if (p->get_status() == state::A) { // state A
				++A;
			}
			else if (p->get_status() == state::I) { // state I
				++I;
			}
			else if (p->get_status() == state::H) { // state H
				++H;
			}
			else if (p->get_status() == state::R) { // state R
				++R;
			}
		}

		res.clients = { S, E, A, I, H, R };

		// reset and do for caregivers

		S = 0, E = 0, A = 0, I = 0, H = 0, R = 0;

		for (const auto& p : people.at(care)) { // look in caregivers
			if (p->get_status() == state::S) { // state S
				++S;
			}
			else if (p->get_status() == state::E) { // state E
				++E;
			}
			else if (p->get_status() == state::A) { // state A
				++A;
			}
			else if (p->get_status() == state::I) { // state I
				++I;
			}
			else if (p->get_status() == state::H) { // state H
				++H;
			}
			else if (p->get_status() == state::R) { // state R
				++R;
			}
		}

		res.caregivers = { S, E, A, I, H, R };

		S = 0, E = 0, A = 0, I = 0, H = 0, R = 0;

		for (const auto& p : people.at(ess)) { // essentials 
			if (p->get_status() == state::S) { // state S
				++S;
			}
			else if (p->get_status() == state::E) { // state E
				++E;
			}
			else if (p->get_status() == state::A) { // state A
				++A;
			}
			else if (p->get_status() == state::I) { // state I
				++I;
			}
			else if (p->get_status() == state::H) { // state H
				++H;
			}
			else if (p->get_status() == state::R) { // state R
				++R;
			}
		}

		res.essential = { S, E, A, I, H, R };

		return res;
	}


	state graph::node::get_status() const {
		return status;
	}


	graph::edge::edge(node* _contact, double _weight, bool _active) :
		contact(_contact), weight(_weight), active(_active) {}

	bool graph::edge::is_active() const {
		return active;
	}

	void graph::edge::set_active_status(bool _active) {
		active = _active;
	}

	void graph::node::break_weak() {
		for (auto& w : weak_contacts) { // go over all the weak contacts
			if (w.second.is_active()) { // need to deactive then
				w.second.set_active_status(false);
				w.second.contact->turn_off_weak(this);
			}
		}
	}

	void graph::node::turn_off_weak(const node* other) {
		// find the connection pointing to other
		try { // find a match
			auto& it = weak_contacts.at(other->get_id());
			// it's there!
			it.set_active_status(false);
		}
		catch (...) {} // or do nothing
	}

	void graph::node::break_strong() {
		for (auto& s : strong_contacts) { // go over all the strong contacts
			if (s.second.is_active()) { // need to deactivate
				s.second.set_active_status(false);
				s.second.contact->turn_off_strong(this);
			}
		}
	}

	void graph::node::turn_off_strong(const node* other) {
		// find the connection pointing to other
		try {
			auto it = strong_contacts.at(other->get_id());
			// found them
			it.set_active_status(false);
		}
		catch (...) {
			// do nothing
		}
	}

	bool graph::edge::points_to(const node* other) const {
		return contact == other;
	}

	const graph::node* graph::edge::points_to() const {
		return contact;
	}

	graph::node* graph::edge::points_to() {
		return contact;
	}

	void graph::node::go_to_hospital() {
		// all contacts broken if in hospital
		break_weak();
		break_strong();
	}

	void graph::node::calculate_pr_not() {
		pr_not = 1;

		// still suscrptible, only calculate intensity for these people
		if (status == state::S) {

			for (const auto& w : weak_contacts) { // do weak first
				if (w.second.is_active() && w.second.contact->is_contagious()) { // so interact, add the intensity

					double rescale = 1;
					double ppe_factor = 1;

					if (get_structure().all_weak_ppe) { // all weak interactions with PPE
						ppe_factor = get_structure().ppe_reduction;
						rescale *= std::pow(ppe_factor, 2);
					}
					else if (get_structure().essentials_wear_ppe) { // if essentials were PPE
						ppe_factor = get_structure().ppe_reduction;
						auto other_e = dynamic_cast<const essential_worker*>(w.second.points_to());
						auto this_e = dynamic_cast<const essential_worker*>(this);
						if (other_e || this_e) { // if either essential, both must use ppe
							rescale *= std::pow(ppe_factor, 2);
						}
					}

					pr_not *= (1 - rescale * w.second.weight * g->settings.beta_S_to_E);
				}
			}

			for (const auto& s : strong_contacts) { // now strong
				if (s.second.is_active() && s.second.contact->is_contagious()) { // so interact, add the intensity
					pr_not *= (1 - s.second.weight * g->settings.beta_S_to_E);
				}
			}
		}

	}


	void graph::node::assign_family_unit(std::shared_ptr<family_unit> fam) {
		f = fam;
	}

	bool graph::node::been_confirmed_positive() const {
		return confirmed_positive;
	}

	size_t graph::node::get_target_strong_size() const {
		return  target_strong_size;
	}

	size_t graph::node::get_target_weak_size() const {
		return target_weak_size;
	}

	size_t graph::node::get_weak_contact_count() const {
		return weak_contacts.size();
	}

	size_t graph::node::get_strong_contact_count() const {
		return strong_contacts.size();
	}

	void graph::node::assign_target_strong_size(size_t _new_size) {
		target_strong_size = _new_size;
	}

	void graph::node::assign_target_weak_size(size_t _new_size) {
		target_weak_size = _new_size;
	}

	void graph::set_weak_strong_target_counts() {
		set_weak_target_counts();
		set_strong_target_counts();
	}

	void graph::node::clear_excess_weak() {

		size_t to_remove = 0;

		// if they do have too many
		if (weak_contacts.size() > target_weak_size) {
			to_remove = weak_contacts.size() - target_weak_size;
		}

		while (to_remove > 0) { // as long as too many

			// shift by random amount from beginning
			auto it = std::begin(weak_contacts);
			std::uniform_int_distribution<size_t> u{ 0, weak_contacts.size() - 1 };

			std::advance(it, u(eng));

			auto* other = it->second.points_to(); // who they point to

			// neither are essential
			if (!dynamic_cast<const essential_worker*>(other) && !dynamic_cast<const essential_worker*>(this)) {
				it->second.points_to()->remove_weak_contact(this);

				weak_contacts.erase(it);
			}

			--to_remove; // one less to remove
		}
	}

	void graph::node::remove_weak_contact(node* other) {
		weak_contacts.erase(other->get_id());
	}

	size_t graph::node::get_id() const {
		return id;
	}

	void graph::set_strong_target_counts() {
		for (auto& pop : people) { // all groups
			for (auto& p : pop.second) { // go to each person
				if (pop.first == gen) { // general
					p->assign_target_strong_size(settings.general_strong_dist->operator()());
				}
				else if (pop.first == ess) { // essential
					p->assign_target_strong_size(settings.essential_strong_dist->operator()());
				}
				else if (pop.first == dis) { // client/disabled
					p->assign_target_strong_size(settings.client_strong_dist->operator()());
				}
				else if (pop.first == care) { // caregiver
					p->assign_target_strong_size(settings.caregiver_strong_dist->operator()());
				}
				else { // not good
					throw std::logic_error("not a valid population");
				}
			}
		}
	}


	void graph::set_weak_target_counts() {
		for (auto& pop : people) { // all groups
			for (auto& p : pop.second) { // go to each person
				if (settings.general_weak_dist && pop.first == gen) { // general
					p->assign_target_weak_size(settings.general_weak_dist->operator()());
				}
				else if (settings.essential_weak_dist && pop.first == ess) { // essential
					p->assign_target_weak_size(settings.essential_weak_dist->operator()());
				}
				else if (settings.client_weak_dist && pop.first == dis) { // client/disabled
					p->assign_target_weak_size(settings.client_weak_dist->operator()());
				}
				else if (settings.caregiver_weak_dist && pop.first == care) { // caregiver
					p->assign_target_weak_size(settings.caregiver_weak_dist->operator()());
				}
			}
		}
	}


	void graph::node::reset_contacts() {

		for (auto& w : weak_contacts) { // go over all the weak contacts

			// either contact has no symptoms or the contact is ill but won't break weak
			if (no_symptoms() &&
				(w.second.contact->no_symptoms() ||
					((w.second.contact->get_status() == state::I) && !w.second.contact->will_break_weak)
					)
				) { // both must be symptom free
				w.second.set_active_status(true);
				w.second.contact->turn_on_weak(this);
			}
		}

		for (auto& s : strong_contacts) { // go over all the weak contacts
										  // person is recovered, other not in hosptial
			if (no_symptoms() && (s.second.contact->get_status() != state::H)) {
				s.second.set_active_status(true);
				s.second.contact->turn_on_strong(this);
			}
		}

	}

	void graph::node::turn_on_weak(const node* other) {
		try { // find that edge
			auto& ed = weak_contacts.at(other->get_id());
			ed.set_active_status(true);
		}
		catch (...) {
			// do nothing
		}
	}

	void graph::node::turn_on_strong(const node* other) {
		try { // find the edge
			auto& ed = strong_contacts.at(other->get_id());
			ed.set_active_status(true);
		}
		catch (...) {
			// should not throw
		}
	}

	bool graph::node::is_contact(const node& other) const {
		return weak_contacts.count(other.get_id()) || strong_contacts.count(other.get_id());
	}

	graph::node::node(const graph* _g,
		double _pr_break_weak) : g(_g), status(state::S),
		target_strong_size(0), target_weak_size(0),
		will_break_weak([this, _pr_break_weak]()->bool {
		// random int 0 or 1
		std::uniform_real_distribution<> rd_01{ 0,1 };
		return rd_01(eng) < _pr_break_weak;
			}()), pr_not(1),
				tested_if_ill(([this, _pr_break_weak]()->bool {
				// random int 0 or 1
				std::uniform_real_distribution<> rd_01{ 0,1 };
				return rd_01(eng) < g->settings.pr_tested_if_ill;
					}())), confirmed_positive(false), id(next_id++) {}

					graph::client::client(const graph* _g, double _pr_break_weak, bool _wears_ppe)
						: node(_g, _pr_break_weak), wear_ppe_w_caregiver(_wears_ppe) {}

					graph::caregiver::caregiver(const graph* _g,
						double _pr_break_weak, bool _wears_ppe) : node(_g, _pr_break_weak),
						wear_ppe_w_client(_wears_ppe) {}

					bool graph::node::no_symptoms() const {
						return (status == state::S) || (status == state::E) || (status == state::A) || (status == state::R);
					}

					bool graph::node::is_contagious() const {
						return (status == state::A) || (status == state::I) || (status == state::H);
					}



					bool graph::caregiver::is_contact(const node& other) const {
						if (!node::is_contact(other)) { // only do a check if not already found

							try { // see if the client is theirs
								const client& c = dynamic_cast<const client&>(other);
								return is_client(c); // check if a client then
							}
							catch (...) { // false if they are not even a client
								return false;
							}
						}
						return true;
					}

					bool graph::caregiver::is_client(const client& other) const {
						// find client
						try { // if they are found, just whether they are active
							const auto& ed = clients.at(other.get_id());
							return ed.is_active();
						}
						catch (...) { // not found
							return false;
						}
					}

					bool graph::client::is_contact(const node& other) const {
						if (!node::is_contact(other)) { // only do a check if not already found

							try { // see if the caregiver is theirs
								const caregiver& c = dynamic_cast<const caregiver&>(other);
								return is_caregiver(c); // check if a caregiver then
							}
							catch (...) { // false if they are not even a caregiver
								return false;
							}

						}

						return true; // ...
					}


					bool graph::client::is_caregiver(const caregiver& other) const {
						// find caregiver
						try {
							const auto& ed = weak_caregivers.at(other.get_id());
							return ed.is_active();
						}
						catch (...) {
							// do nothing
						}

						try {
							const auto& ed = strong_caregivers.at(other.get_id());
							return ed.is_active();
						}
						catch (...) {
							// do nothing
						}


						// should never get here but okay...
						return false;
					}


					void graph::node::scale_not(double _factor) {
						pr_not *= _factor;
					}

					void graph::caregiver::calculate_pr_not() {
						node::calculate_pr_not(); // do the node calculations first

						scale_not(extra_caregiver_factor());
					}

					double graph::caregiver::extra_caregiver_factor() const {
						double extra = 1;

						// only do intensity of susceptible
						if (get_status() == state::S) {
							for (const auto& c : clients) { // do weak first
								if (c.second.is_active() && c.second.contact->is_contagious()) { // so interact, add the intensity
									const client* the_client = dynamic_cast<const client*>(c.second.points_to());


									// they are a client AND they are currently receiving care from this person
									if (the_client && the_client->is_receiving_care_from(this)) {
										double reduction = 1;
										if (ppe_worn_with_client()) { // they wear ppe
											reduction *= get_structure().ppe_reduction;
										}
										if (the_client->ppe_worn_with_caregiver()) { // discount for ppe of client
											reduction *= get_structure().ppe_reduction;
										}
										extra *= (1 - reduction * c.second.weight * get_graph()->settings.beta_S_to_E);
									}
								}
							}
						}

						return extra;
					}

					void graph::client::calculate_pr_not() {
						node::calculate_pr_not(); // do the node calculations first

						scale_not(extra_client_factor());
					}

					double graph::client::extra_client_factor() const {
						double extra = 1;

						// still susceptible
						if (get_status() == state::S) {

							for (const auto& c : daily) { // look at those assigned today
								if (c.second.is_active() && c.second.contact->is_contagious()) { // so interact, add the intensity
									const caregiver* the_caregiver = dynamic_cast<const caregiver*>(c.second.points_to());

									if (the_caregiver) { // they are a caregiver
										double reduction = 1;

										if (ppe_worn_with_caregiver()) { // they wear ppe
											reduction *= get_structure().ppe_reduction;
										}
										if (the_caregiver->ppe_worn_with_client()) { // discount for ppe of caregiver
											reduction *= get_structure().ppe_reduction;
										}



										extra *= (1 - reduction * c.second.weight * get_graph()->settings.beta_S_to_E);
									}
								}
							}

							for (const auto& c : strong_caregivers) { // strong connections
								if (c.second.is_active() && c.second.contact->is_contagious()) { // so interact, add the intensity
									const caregiver* the_caregiver = dynamic_cast<const caregiver*>(c.second.points_to());
									if (the_caregiver) { // they are a caregiver
										double reduction = 1;

										if (ppe_worn_with_caregiver()) { // they wear ppe
											reduction *= get_structure().ppe_reduction;
										}
										if (the_caregiver->ppe_worn_with_client()) { // discount for ppe of caregiver
											reduction *= get_structure().ppe_reduction;
										}

										extra *= (1 - reduction * c.second.weight * get_graph()->settings.beta_S_to_E);
									}
								}
							}

						}

						return extra;
					}

					void graph::client::break_weak() {

						node::break_weak();

						break_weak_caregivers();
					}

					void graph::client::break_weak_caregivers() {

						for (auto& c : weak_caregivers) { // go over all the weak contacts
							if (c.second.is_active()) { // need to deactive then
								c.second.set_active_status(false);

								// c.contact is a pointer to node
								caregiver* the_caregiver = dynamic_cast<caregiver*>(c.second.contact);
								if (the_caregiver) { // can turn off client if a caregiver
									the_caregiver->turn_off_client(this);
								}
							}
						}

					}

					void graph::client::break_strong() {
						node::break_strong();

						break_strong_caregivers();
					}

					void graph::client::break_strong_caregivers() {

						for (auto& c : strong_caregivers) { // go over all the weak contacts
							if (c.second.is_active()) { // need to deactive then
								c.second.set_active_status(false);

								// c.contact is a pointer to node
								caregiver* the_caregiver = dynamic_cast<caregiver*>(c.second.contact);
								if (the_caregiver) { // can turn off client if a caregiver
									the_caregiver->turn_off_client(this);
								}
							}
						}


					}

					void graph::caregiver::break_weak() {

						node::break_weak();

						break_clients();
					}

					void graph::caregiver::break_strong() {

						// in case connections not already broken,
						// will break them b/c going to hospital
						break_weak();
					}


					void graph::caregiver::break_clients() {

						for (auto& c : clients) { // go over all the weak contacts
							if (c.second.is_active()) { // need to deactive then
								c.second.set_active_status(false);

								// c.contact is a pointer to node
								client* the_client = dynamic_cast<client*>(c.second.contact);
								if (the_client) { // can turn off client if a caregiver
									the_client->turn_off_caregiver(this);
								}
							}
						}

					}


					void graph::node::add_strong_contact(node& other, double _weight) {

						strong_contacts[other.get_id()] = { &other, _weight };

					}

					void graph::node::remove_strong_contact(node& other, bool last_step) {

						auto it = strong_contacts.find(other.get_id());

						if (it != std::end(strong_contacts)) { // so we can derefernece it
							if (last_step) { // just clean up other
								strong_contacts.erase(it);
							}
							else { // go on to the last step
								auto* ptr = it->second.points_to(); // person to remove this from
								strong_contacts.erase(it);
								ptr->remove_strong_contact(*this, true);
							}
						}

					}

					std::optional<const graph::client*> graph::node::get_disabled_family() const {
						for (const auto& s : strong_contacts) { // look over family
							const node* n = s.second.points_to();

							if (const client* c = dynamic_cast<const client*>(n)) { // find who is disabled
								return c;
							}
						}
						return std::nullopt;
					}

					void graph::node::add_weak_contact(node& other, double _weight) {

						weak_contacts[other.get_id()] = { &other, _weight };

					}

					void graph::node::clear_weak() {
						weak_contacts.clear();
					}

					void graph::client::add_weak_caregiver(caregiver& other, double _weight) {
						weak_caregivers[other.get_id()] = { &other, _weight };
					}



					void graph::client::add_strong_caregiver(caregiver& other, double _weight) {

						strong_caregivers[other.get_id()] = { &other, _weight };

					}



					void graph::caregiver::add_client(client& other, double _weight) {
						clients[other.get_id()] = { &other, _weight };
					}



					void graph::client::reset_contacts() {

						node::reset_contacts(); // do the node stuff first

						reset_caregivers();
					}

					void graph::client::reset_caregivers() {

						for (auto& c : weak_caregivers) { // go over all the caregivers
							if (no_symptoms() && c.second.contact->no_symptoms()) { // both symptom free
								c.second.set_active_status(true); // make both edges active
								caregiver* the_caregiver = dynamic_cast<caregiver*>(c.second.contact);
								if (the_caregiver) { // can work on this object
									the_caregiver->turn_on_client(this);
								}
							}
						}

						for (auto& c : strong_caregivers) { // go over all the caregivers
							if (no_symptoms() && c.second.contact->no_symptoms()) { // both symptom free
								c.second.set_active_status(true); // make both edges active
								caregiver* the_caregiver = dynamic_cast<caregiver*>(c.second.contact);
								if (the_caregiver) { // can work on this object
									the_caregiver->turn_on_client(this);
								}
							}
						}

					}

					void graph::caregiver::reset_contacts() {

						node::reset_contacts(); // do the node stuff first

						reset_clients();
					}

					void graph::caregiver::reset_clients() {

						for (auto& c : clients) { // go over all the clients
							if (no_symptoms() && c.second.contact->no_symptoms()) { // both are symptom free
								c.second.set_active_status(true); // make both edges active
								client* the_client = dynamic_cast<client*>(c.second.contact);
								if (the_client) { // can work on this object
									the_client->turn_on_caregiver(this);
								}
							}
						}

					}


					void graph::client::turn_off_caregiver(const node* other) {

						auto it = weak_caregivers.find(other->get_id());

						if (it != std::end(weak_caregivers)) { // find other as a weak caregiver
							it->second.set_active_status(false);
							return;
						}

						it = strong_caregivers.find(other->get_id());

						if (it != std::end(strong_caregivers)) { // find other as a strong caregiver
							it->second.set_active_status(false);
						}

					}

					void graph::caregiver::turn_off_client(const node* other) {

						auto it = clients.find(other->get_id());

						if (it != std::end(clients)) { // find the client
							it->second.set_active_status(false);
						}
					}

					void graph::node::make_ill(state disease_state) {
						status = disease_state;
					}

					bool graph::node::breaks_weak() const {
						return will_break_weak;
					}

					graph::cumulative_outcome graph::node::evolve(double dT) {
						// assume intensity is already set!

						cumulative_outcome res = cumulative_outcome::other;

						if (status == state::S) { // susceptible
												  // total intensity rate
							const double p_get = 1 - pr_not;

							std::uniform_real_distribution<> get_0_1(0, 1);

							if (get_0_1(eng) < p_get) { // they get sick this time
								status = state::E;
							}
						}
						else if (status == state::E) { // exposed 
							// might become infectious
							std::exponential_distribution<double> to_a(g->settings.nu_E_to_A);

							if (to_a(eng) < dT) { // they get to recover
								status = state::A;
							}
						}
						else if (status == state::A) { // asymptomatic
							std::exponential_distribution<double> to_i_rand(g->settings.alpha_A_to_I);
							std::exponential_distribution<double> to_r_rand(g->settings.eta_A_to_R);

							double dT_ill = to_i_rand(eng);
							double dT_rec = to_r_rand(eng);

							if (dT_ill < dT_rec) { // ill might happen
								if (dT_ill < dT) { // they do get ill
									status = state::I;
									res = cumulative_outcome::I_inc;

									if (will_break_weak) { // break only if they will
										break_weak();
									}
									
									// this person will have a positive tested
									if (!confirmed_positive && tested_if_ill) {
										confirmed_positive = true;
									}
								}
							}
							else { // might recover
								if (dT_rec < dT) { // they do recover
									status = state::R;
									res = cumulative_outcome::R_inc;
									reset_contacts();
								}
							}
						}

						else if (status == state::I) { // ill

							std::exponential_distribution<double> to_h_rand(g->settings.mu_I_to_H);
							std::exponential_distribution<double> to_r_rand(g->settings.rho_I_to_R);

							double dT_hosp = to_h_rand(eng);
							double dT_recov = to_r_rand(eng);

							if (dT_hosp < dT_recov) { // hospital comes first
								if (dT_hosp < dT) { // go to hospital
									status = state::H;
									res = cumulative_outcome::H_inc;
									go_to_hospital();									
									
									// have to be confirmed in hospital
									if (!confirmed_positive) {
										confirmed_positive = true;
									}
								}
							}
							else { // recovery first
								if (dT_recov < dT) { // to recover
									status = state::R;
									res = cumulative_outcome::R_inc;
									reset_contacts();
								}
							}
						}
						else if (status == state::H) { // in hospital							

							// might recover
							std::exponential_distribution<double> to_r(g->settings.zeta_H_to_R);

							if (to_r(eng) < dT) { // they get to recover
								status = state::R;
								res = cumulative_outcome::R_inc;
								reset_contacts();
							}
						}
						if (status == state::R) { // recovered

							// if rate R->S is 0, don't care
							// generating exp distribution from 0 throws exception
							if (g->settings.gamma_R_to_S != 0) { // will be valid here
								std::exponential_distribution<double> susc_rand(g->settings.gamma_R_to_S);

								if (susc_rand(eng) < dT) { // they are susceptible again
									status = state::S;
								}
							}
						}

						return res;

					}



					void graph::caregiver::turn_on_client(const node* other) {

						auto it = clients.find(other->get_id());

						if (it != std::end(clients)) { // they were found
							it->second.set_active_status(true);
						}

					}


					void graph::client::turn_on_caregiver(const node* other) {
						auto it = weak_caregivers.find(other->get_id());

						if (it != std::end(weak_caregivers)) { // find other as a weak caregiver
							it->second.set_active_status(true);
							return;
						}

						it = strong_caregivers.find(other->get_id());

						if (it != std::end(strong_caregivers)) { // find other as a strong caregiver
							it->second.set_active_status(true);
						}

					}

					graph::edge::edge() : contact(nullptr), weight(0), active(false) {}

					void graph::edge::set_weight(double _weight) {
						weight = _weight;
					}

					std::vector<size_t> graph::generate_weak_ids() const {
						std::vector< size_t > weak_ids;

						weak_ids.reserve(settings.total_population * settings.essential_weak_dist->get_mean());

						size_t curr_index = 0; // track id of people

						try { // possibly general population not present
							// for each general person
							for (size_t i = 0, gen_count = people.at(gen).size(); i < gen_count; ++i) {

								// they have this many weak contacts as a target
								size_t weak_target = people.at(gen)[i]->get_target_weak_size();

								// they need this many now
								size_t now = people.at(gen)[i]->get_weak_contact_count();

								size_t needed = ((now < weak_target) ? (weak_target - now) : 0);

								for (size_t j = 0; j < needed; ++j) { // add in as many as are in deficit
									weak_ids.push_back(curr_index);
								}
								++curr_index; // increment only after each person gets their stubs
							}
						}
						catch (const std::out_of_range& e) { /* don't need to do anything */ }

						try {
							// for each caregiver person
							for (size_t i = 0, care_size = people.at(care).size(); i < care_size;
								++i) {
								// they have this many weak contacts as a target
								size_t weak_target = people.at(care)[i]->get_target_weak_size();

								// they need this many now
								size_t now = people.at(care)[i]->get_weak_contact_count();

								size_t needed = ((now < weak_target) ? (weak_target - now) : 0);

								for (size_t j = 0; j < needed; ++j) { // add in as many as are in deficit
									weak_ids.push_back(curr_index);
								}
								++curr_index; // increment only after each person gets their stubs
							}
						}
						catch (const std::out_of_range& e) { /* don't need to do anything */ }

						try {
							// for each client person
							for (size_t i = 0, client_size = people.at(dis).size(); i < client_size; ++i) {
								// they have this many weak contacts as a target
								size_t weak_target = people.at(dis)[i]->get_target_weak_size();

								// they need this many now
								size_t now = people.at(dis)[i]->get_weak_contact_count();

								size_t needed = ((now < weak_target) ? (weak_target - now) : 0);

								for (size_t j = 0; j < needed; ++j) { // add in as many as are in deficit
									weak_ids.push_back(curr_index);
								}
								++curr_index; // increment only after each person gets their stubs
							}
						}
						catch (const std::out_of_range& e) { /* don't need to do anything */ }


						try {
							// for each essential person
							for (size_t i = 0, essential_size = people.at(ess).size(); i < essential_size; ++i) {
								// they have this many weak contacts as a target
								size_t weak_target = people.at(ess)[i]->get_target_weak_size();

								// they need this many now
								size_t now = people.at(ess)[i]->get_weak_contact_count();

								size_t needed = ((now < weak_target) ? (weak_target - now) : 0);

								for (size_t j = 0; j < needed; ++j) { // add in as many as are in deficit
									weak_ids.push_back(curr_index);
								}
								++curr_index; // increment only after each person gets their stubs
							}
						}
						catch (const std::out_of_range& e) { /* don't need to do anything */ }

						return weak_ids;
					}


					std::vector< std::vector<size_t> > graph::generate_strong_ids() const {
						std::vector< std::vector<size_t> > strong_ids;

						thread_local std::default_random_engine eng{ std::random_device()() };

						// how many people there are
						size_t total_population = 0;
						for (const auto& pair : people) {
							total_population += pair.second.size();
						}

						std::vector<size_t> all_ids(total_population);

						for (size_t i = 0; i < total_population; ++i) { // set all the ids
							all_ids[i] = i;
						}


						while (!all_ids.empty()) { // until all assigned
							std::uniform_int_distribution<size_t> id_gen(0, all_ids.size() - 1);

							size_t rand_pos = id_gen(eng); // position in list of ids
							size_t rand_ind = all_ids[rand_pos]; // actual id
							all_ids.erase(all_ids.begin() + rand_pos); // remove so can't double-up

							// family representative
							auto& rep = person_finder(rand_ind);

							std::vector<size_t> family_ids;
							family_ids.push_back(rand_ind); // store rep's id

							size_t others = rep->get_target_strong_size();


							for (size_t fam_size = 0; fam_size < others; ++fam_size) {

								if (all_ids.empty()) { // can't do any more
									break;
								}

								std::uniform_int_distribution<size_t> id_gen{ 0, all_ids.size() - 1 };

								size_t rand_pos = id_gen(eng); // position in list of ids
								size_t rand_ind = all_ids[rand_pos]; // actual id

								family_ids.push_back(rand_ind);

								all_ids.erase(all_ids.begin() + rand_pos); // remove so can't double-up
							}

							strong_ids.push_back(family_ids); // each element of this will be a family of ids
						}

						return strong_ids;
					}


					void graph::redistribute_weak(Open_Close oc) {

						set_weak_target_counts();

						// now we do removals so no one has more than their target
						for (auto& pop : people) { // each subpopulation
							for (auto& p : pop.second) { // look at each person
								p->clear_excess_weak(); // remove weak contacts if necessary
							}
						}

						if (oc == Open_Close::OPEN) { // opening: set the distributions
							settings.general_weak_dist = settings.open.strat_general_weak_dist;
							settings.caregiver_weak_dist = settings.open.strat_caregiver_weak_dist;
							settings.client_weak_dist = settings.open.strat_client_weak_dist;
							settings.essential_weak_dist = settings.open.strat_essential_weak_dist;
						}
						else { // then it's closing: set the distributions
							settings.general_weak_dist = settings.close.strat_general_weak_dist;
							settings.caregiver_weak_dist = settings.close.strat_caregiver_weak_dist;
							settings.client_weak_dist = settings.close.strat_client_weak_dist;
							settings.essential_weak_dist = settings.close.strat_essential_weak_dist;
						}

						if (oc == Open_Close::OPEN) { // in case of opening, more weak contacts to add							
							auto ids = generate_weak_ids(); // generate the ids
							assign_weak(std::move(ids));
						}
					}



					std::unique_ptr<graph::node>& graph::person_finder(size_t i) {
						// defer to the const case
						return const_cast<std::unique_ptr<graph::node>&>(
							static_cast<const graph*>(this)->person_finder(i));

					}

					const std::unique_ptr<graph::node>& graph::person_finder(size_t i) const {
						if (i < gen_ptr.first) { // in general range
							return (*gen_ptr.second)[i];
						}
						i -= gen_ptr.first; // or move to caregiver range
						if (i < care_ptr.first) { // in caregiver range
							return (*care_ptr.second)[i];
						}
						i -= care_ptr.first; // or move to disabled range
						if (i < dis_ptr.first) { // in disbled range
							return (*dis_ptr.second)[i];
						}
						i -= dis_ptr.first; // or move to essential range
						return (*ess_ptr.second)[i];

					}

					std::unique_ptr<graph::node>& graph::caregiver_finder(size_t i) {
						return people.at(care)[i];
					}

					void graph::assign_weak(std::vector<size_t> weak_ids) {
						const double wt = settings.weak_edge_weight;

						// the index where beyond which all contacts have been paired up
						size_t placed_marker_weak = weak_ids.size();

						// until everyone paired up or one stub leftover
						while (placed_marker_weak > 1) {
							std::uniform_int_distribution<size_t> rd_i{ 0,
								placed_marker_weak - 1 };

							size_t rand1 = rd_i(eng);
							size_t rand2 = rd_i(eng);

							// we now pair the people up
							// pick the people
							auto& p1 = person_finder(weak_ids[rand1]);
							auto& p2 = person_finder(weak_ids[rand2]);

							// add them as contacts of each other
							// if they are not the same person and not
							// already contacts, add each other
							if ((weak_ids[rand1] != weak_ids[rand2]) &&
								!(p1->is_contact(*p2))) {
								p1->add_weak_contact(*p2, wt);
								p2->add_weak_contact(*p1, wt);
							}

							// put the selected values at the end of the
							// vector and updated the placed position
							// accordingly
							std::swap(weak_ids[rand1],
								weak_ids[placed_marker_weak - 1]);
							--placed_marker_weak;

							std::swap(weak_ids[rand2],
								weak_ids[placed_marker_weak - 1]);
							--placed_marker_weak;


						}
					}

					void graph::assign_strong(std::vector< std::vector<size_t> > strong_ids) {
						const double wt = settings.strong_edge_weight;

						for (const auto& fam : strong_ids) { // each will be list of ids for a family

							auto fam_unit = std::make_shared<family_unit>();

							for (size_t i = 0; i < fam.size(); ++i) { // go over every person here

								auto& p1 = person_finder(fam[i]);
								p1->assign_family_unit(fam_unit);

								for (size_t j = i + 1; j < fam.size(); ++j) { // the people to add

									auto& p2 = person_finder(fam[j]);

									if (!(p1->is_contact(*p2))) { // make sure not already contact - should not happen
										p1->add_strong_contact(*p2, wt);
										p2->add_strong_contact(*p1, wt);
										p2->assign_family_unit(fam_unit);
									}
								}
							}
						}


					}

					void graph::make_some_asymptomatic_or_ill() {

						// if they are a list of numbers
						if (auto nums = std::dynamic_pointer_cast<numbers_infected>(settings.initial_infected_values)) {
							make_numbers_asymptomatic(nums->get_vec_asymptomatic());
							make_numbers_ill(nums->get_vec_ill());
						}
						else { // must be a fraction type
							auto frac = std::dynamic_pointer_cast<fraction_infected>(settings.initial_infected_values);
							make_fraction_asymptomatic(*frac);
						}


					}


					void graph::make_fraction_asymptomatic(double frac) {
						// will pick index for person in population
						std::uniform_int_distribution<size_t>
							rd_i{ 0, settings.total_population - 1 };

						// how many infected to start
						size_t initial_infected = static_cast<size_t>(settings.total_population *
							frac + 0.5);

						// make a few ill
						for (size_t i = 0; i < initial_infected; ++i) {
							auto& person = person_finder(rd_i(eng));
							person->make_ill(state::A);
						}
					}


					void graph::make_numbers_asymptomatic(const std::vector<size_t>& nums) {

						const size_t num_gen = nums[0];
						const size_t num_client = nums[1];
						const size_t num_care = nums[2];
						const size_t num_ess = nums[3];

						std::uniform_int_distribution<size_t>
							rd_i{ 0, people.at(gen).size() - 1 };

						for (size_t i = 0; i < num_gen; ++i) {
							size_t rand = rd_i(eng);
							people.at(gen)[rand]->make_ill(state::A);
						}

						rd_i = std::uniform_int_distribution<size_t>{ 0, people.at(dis).size() - 1 };
						for (size_t i = 0; i < num_client; ++i) {
							size_t rand = rd_i(eng);
							people.at(dis)[rand]->make_ill(state::A);
						}

						rd_i = std::uniform_int_distribution<size_t>{ 0, people.at(care).size() - 1 };
						for (size_t i = 0; i < num_care; ++i) {
							size_t rand = rd_i(eng);
							people.at(care)[rand]->make_ill(state::A);
						}

						rd_i = std::uniform_int_distribution<size_t>{ 0, people.at(ess).size() - 1 };
						for (size_t i = 0; i < num_ess; ++i) {
							size_t rand = rd_i(eng);
							people.at(ess)[rand]->make_ill(state::A);
						}

					}


					void graph::make_numbers_ill(const std::vector<size_t>& nums) {

						const size_t num_gen = nums[0];
						const size_t num_client = nums[1];
						const size_t num_care = nums[2];
						const size_t num_ess = nums[3];

						std::uniform_int_distribution<size_t>
							rd_i{ 0, people.at(gen).size() - 1 };

						for (size_t i = 0; i < num_gen; ++i) {
							size_t rand = rd_i(eng);
							people.at(gen)[rand]->make_ill(state::I);
							if (people.at(gen)[rand]->breaks_weak()) {
								people.at(gen)[rand]->break_weak();
							}
						}

						rd_i = std::uniform_int_distribution<size_t>{ 0, people.at(dis).size() - 1 };
						for (size_t i = 0; i < num_client; ++i) {
							size_t rand = rd_i(eng);
							people.at(dis)[rand]->make_ill(state::I);
							if (people.at(dis)[rand]->breaks_weak()) {
								people.at(dis)[rand]->break_weak();
							}
						}

						rd_i = std::uniform_int_distribution<size_t>{ 0, people.at(care).size() - 1 };
						for (size_t i = 0; i < num_care; ++i) {
							size_t rand = rd_i(eng);
							people.at(care)[rand]->make_ill(state::I);
							if (people.at(care)[rand]->breaks_weak()) {
								people.at(care)[rand]->break_weak();
							}
						}

						rd_i = std::uniform_int_distribution<size_t>{ 0, people.at(ess).size() - 1 };
						for (size_t i = 0; i < num_ess; ++i) {
							size_t rand = rd_i(eng);
							people.at(ess)[rand]->make_ill(state::I);
							if (people.at(ess)[rand]->breaks_weak()) {
								people.at(ess)[rand]->break_weak();
							}
						}

					}


					void graph::pair_clients_and_caregivers() {

						const double wt = settings.caregiver_client_weight;

						std::uniform_int_distribution<size_t> rd_care{ 0,
							people[care].size() - 1 };

						try {
							for (auto& c : people.at(dis)) { // each c is really a client

								if (client* the_client = dynamic_cast<client*>(c.get())) { // do the casting to caregiver

									size_t weak_caregivers_per_client_dist = (*settings.weak_caregivers_per_client_dist)();

									for (size_t i = 0; i < weak_caregivers_per_client_dist; ++i) { // add caregivers
										size_t place = rd_care(eng);
										if (caregiver* the_caregiver =
											dynamic_cast<caregiver*>(caregiver_finder(place).get())) { // and to caregiver

																									   // they add each other if not already contacts
																									   // weak caregiver
											if (!(the_client->is_contact(*the_caregiver))) {
												the_client->add_weak_caregiver(*the_caregiver, wt);
												the_caregiver->add_client(*the_client, wt);
											}
										}
									}


									size_t strong_caregivers_per_client_dist = (*settings.strong_caregivers_per_client_dist)();
									for (size_t i = 0; i < strong_caregivers_per_client_dist; ++i) { // add caregivers
										size_t place = rd_care(eng);

										if (caregiver* the_caregiver =
											dynamic_cast<caregiver*>(caregiver_finder(place).get())) { // and to caregiver

																									   // they add each other if not already contacts
																									   // strong caregiver
											if (!(the_client->is_contact(*the_caregiver))) {
												the_client->add_strong_caregiver(*the_caregiver, wt);
												the_caregiver->add_client(*the_client, wt);
											}
										}
									}
								}
							}
						}
						catch (const std::out_of_range& e) { /* don't need to do anything */ }

					}



					void graph::initialize_population() {
						size_t total_general = static_cast<size_t>(settings.total_population * settings.general_percent / 100
							+ 0.5);

						size_t total_clients = static_cast<size_t>(settings.total_population * settings.client_percent / 100
							+ 0.5);

						size_t total_caregivers = static_cast<size_t>(settings.total_population * settings.caregiver_percent / 100
							+ 0.5);

						size_t total_essential = static_cast<size_t>(settings.total_population *
							settings.essential_worker_percent / 100
							+ 0.5);

						// make sure the vectors are at least there, even if empty
						people[gen];
						people[dis];
						people[care];
						people[ess];

						// add in the general population
						for (size_t i = 0; i < total_general; ++i) {
							people[gen].emplace_back(std::make_unique<node>(this,
								settings.pr_break_weak));
						}

						// add in the essential worker population
						for (size_t i = 0; i < total_essential; ++i) {
							people[ess].emplace_back(std::make_unique<essential_worker>(this,
								settings.pr_break_weak));
						}

						// add in the client population
						for (size_t i = 0; i < total_clients; ++i) {
							people[dis].emplace_back(std::make_unique<client>(this,
								settings.pr_break_weak, settings.client_wears_ppe));
						}

						// add in the caregiver population
						for (size_t i = 0; i < total_caregivers; ++i) {
							people[care].emplace_back(std::make_unique<caregiver>(this,
								settings.pr_break_weak, settings.caregiver_wears_ppe));
						}

					}


					void graph::deploy_strategy(const graph_structure::changes& opt, Open_Close oc) {

						/* change the distributions, but only if they are different...
						we do this so that if a group is assigned an identical
						distribution as they previously had, they will not
						be assigned a new number of contacts.
						Otherwise during lockdowns, since we only allow
						contact-breaking, the mean number of contacts won't be signficiantly affected if a group retains its previous
						distribution

						We do change the distribution later, but the nullptr just
						prevents assigning a new number of contacts
						*/

						// general weak distributions are the same so no change 
						if (settings.general_weak_dist->equal(*opt.strat_general_weak_dist)) {
							settings.general_weak_dist = nullptr;
						}
						else { // update distribution
							settings.general_weak_dist = opt.strat_general_weak_dist;
						}

						// essential weak distributions are the same so no change
						if (settings.essential_weak_dist->equal(*opt.strat_essential_weak_dist)) {
							settings.essential_weak_dist = nullptr;
						}
						else { // update distribution
							settings.essential_weak_dist = opt.strat_essential_weak_dist;
						}

						// disabled weak distributions are the same so no change
						if (settings.client_weak_dist->equal(*opt.strat_client_weak_dist)) {
							settings.client_weak_dist = nullptr;
						}
						else { // update distribution
							settings.client_weak_dist = opt.strat_client_weak_dist;
						}

						// caregiver weak distributions are the same so no change
						if (settings.caregiver_weak_dist->equal(*opt.strat_caregiver_weak_dist)) {
							settings.caregiver_weak_dist = nullptr;
						}
						else { // update distribution
							settings.caregiver_weak_dist = opt.strat_caregiver_weak_dist;
						}


						// change ppe strategy
						settings.caregiver_wears_ppe = opt.strat_caregiver_wears_ppe;
						settings.client_wears_ppe = opt.strat_client_wears_ppe;
						settings.essentials_wear_ppe = opt.strat_essentials_wear_ppe;
						settings.all_weak_ppe = opt.strat_all_weak_ppe;

						// for every disabled person
						for (auto& dis : people.at(dis)) {
							// cast to client and assign ppe
							if (client* c = dynamic_cast<client*>(dis.get())) {
								c->set_ppe(settings.client_wears_ppe);
							}
						}

						// for every caregiver person
						for (auto& p : people.at(care)) {
							// cast to client and assign ppe
							if (caregiver* c = dynamic_cast<caregiver*>(p.get())) {
								c->set_ppe(settings.caregiver_wears_ppe);
							}
						}

						redistribute_weak(oc);

					}


					void graph::graph::assign_daily_caregivers() {
						for (auto& d : people[dis]) { // for each disabled
							// cast them to their client status
							if (client* cp = dynamic_cast<client*> (d.get())) {
								cp->assign_chosen_caregivers();
							}
						}
					}


					void graph::client::assign_chosen_caregivers() {

						daily.clear(); // start with no one

						// copy the caregiver ids
						std::vector<size_t> weak_caregiver_ids;

						for (const auto& p : weak_caregivers) {
							weak_caregiver_ids.push_back(p.first);
						}

						// make an engine
						std::default_random_engine e{ std::random_device{}() };

						for (size_t i = 0, len = weak_caregiver_ids.size(); i < len; ++i) {
							std::uniform_int_distribution<size_t> u{ 0, len - 1 };
							std::swap(weak_caregiver_ids[i], weak_caregiver_ids[u(e)]);
						}

						// how many daily caregivers they should have
						size_t target = get_structure().daily_caregivers;

						// now pick people until we run out of options
						for (size_t i = 0; i < target && !weak_caregiver_ids.empty(); ++i) {
							size_t id = weak_caregiver_ids.back();
							weak_caregiver_ids.pop_back();

							if (weak_caregivers.count(id)) { // they are a weak caregiver
								daily[id] = weak_caregivers[id];
							}
							else { // bad logic here
								throw std::runtime_error("not any caregiver for this person!");
							}
						}

					}

					bool graph::client::is_receiving_care_from(const caregiver* c) const {

						// deterines if disabled person receives care from c 
						// if c is among their weak caregivers
						auto c_finder = [c](const decltype(*cbegin(weak_caregivers))& p)->
							bool {
							return p.second.points_to(c);
						};

						// check for both weak caregivers assigned and strong 
						return
							(std::find_if(std::cbegin(daily), std::cend(daily),
								c_finder)
								!= std::cend(daily))
							||
							(std::find_if(std::cbegin(strong_caregivers), std::cend(strong_caregivers),
								c_finder)
								!= std::cend(strong_caregivers));

					}


					size_t graph::client::get_strong_caregiver_count() const {
						return strong_caregivers.size();
					}


					size_t graph::client::get_weak_caregiver_count() const {
						return weak_caregivers.size();
					}


					const graph::graph_structure& graph::node::get_structure() const {
						return g->settings;
					}

					bool graph::caregiver::ppe_worn_with_client() const {
						return wear_ppe_w_client;
					}

					bool graph::client::ppe_worn_with_caregiver() const {
						return wear_ppe_w_caregiver;
					}

					size_t graph::caregiver::num_clients() const {
						return clients.size();
					}

					void graph::caregiver::set_ppe(bool wears_ppe) {
						wear_ppe_w_client = wears_ppe;
					}

					void graph::client::set_ppe(bool wears_ppe) {
						wear_ppe_w_caregiver = wears_ppe;
					}


					graph::essential_worker::essential_worker(const graph* _g,
						double _pr_break_weak) : node(_g, _pr_break_weak) {}

					// next id starts at 0
					thread_local int graph::family_unit::next_id = 0;

					graph::family_unit::family_unit() : id(next_id++) {}

					int graph::family_unit::get_id() const {
						return id;
					}

					graph::graph_structure read_parameters(std::string file_name) {

						std::ifstream fin(std::move(file_name));

						if (!fin) { // could not open the file 
							std::cerr << "A parameter file was not found!\n";
							throw std::runtime_error("parameter file missing");
						}

						std::string line;

						std::map<std::string, covid::graph::graph_structure_reader> values;
						graph::graph_structure settings;

						// how long a line should be at minimum
						const size_t min_line_length = 3;

						// as long as we can read a whole line
						while (std::getline(fin, line)) {

							// either no content or a comment line
							if ((line.size() < min_line_length) || (line[0] == '#')) {
								continue;
							}

							std::istringstream sin(line);

							std::string field;
							std::getline(sin, field, ':'); // will be first item before :

							std::string data;
							std::getline(sin, data, '#'); // scan to here

							covid::graph::graph_structure_reader reader(data);

							// add in the structure reader
							values.emplace(std::make_pair(field, graph::graph_structure_reader(data)));
						}

						fin.close();

						std::string val; // value setting

						using dist_type = std::shared_ptr<integer_distribution<size_t, double>>;

						const double percent_max = 100;

						try { // will throw if not set
							val = "initial_infected_values";
							settings.initial_infected_values = static_cast<std::shared_ptr<initial_infected>> (
								values.at(val));
							val = "total_population";
							settings.total_population = static_cast<size_t> (
								values.at(val));
							val = "client_percent";
							settings.client_percent = static_cast<double> (
								values.at(val));
							val = "caregiver_percent";
							settings.caregiver_percent = static_cast<double> (
								values.at(val));
							val = "essential_worker_percent";
							settings.essential_worker_percent = static_cast<double> (
								values.at(val));
							settings.general_percent = percent_max - settings.client_percent -
								settings.caregiver_percent - settings.essential_worker_percent;
							val = "general_strong_dist";
							settings.general_strong_dist = static_cast<dist_type>(
								values.at(val));
							val = "general_weak_dist";
							settings.general_weak_dist = static_cast<dist_type>(
								values.at(val)
								);
							val = "caregiver_strong_dist";
							settings.caregiver_strong_dist = static_cast<dist_type>(
								values.at(val)
								);
							val = "caregiver_weak_dist";
							settings.caregiver_weak_dist = static_cast<dist_type>(
								values.at(val)
								);
							val = "client_strong_dist";
							settings.client_strong_dist = static_cast<dist_type>(
								values.at(val));
							val = "client_weak_dist";
							settings.client_weak_dist = static_cast<dist_type>(
								values.at(val));
							val = "essential_strong_dist";
							settings.essential_strong_dist = static_cast<dist_type>(
								values.at(val));
							val = "essential_weak_dist";
							settings.essential_weak_dist = static_cast<dist_type>(
								values.at(val)
								);
							val = "weak_caregivers_per_client_dist";
							settings.weak_caregivers_per_client_dist = static_cast<dist_type>(
								values.at(val)
								);
							val = "strong_caregivers_per_client_dist";
							settings.strong_caregivers_per_client_dist = static_cast<dist_type>(
								values.at(val)
								);
							val = "daily_caregivers";
							settings.daily_caregivers = static_cast<size_t>(
								values.at(val)
								);
							val = "pr_break_weak";
							settings.pr_break_weak =
								static_cast<double>(
									values.at(val));
							val = "ppe_reduction";
							settings.ppe_reduction =
								static_cast<double>(
									values.at(val));
							val = "weak_edge_weight";
							settings.weak_edge_weight =
								static_cast<double>(values.at(val));
							val = "strong_edge_weight";
							settings.strong_edge_weight =
								static_cast<double>(
									values.at(val));
							val = "caregiver_client_weight";
							settings.caregiver_client_weight =
								static_cast<double>(
									values.at(val));
							val = "caregiver_wears_ppe";
							settings.caregiver_wears_ppe =
								static_cast<bool>(
									values.at(val));
							val = "client_wears_ppe";
							settings.client_wears_ppe =
								static_cast<bool>(
									values.at(val));
							val = "essentials_wear_ppe";
							settings.essentials_wear_ppe =
								static_cast<bool>(
									values.at(val));
							val = "all_weak_ppe";
							settings.all_weak_ppe =
								static_cast<bool>(
									values.at(val));
							val = "pr_tested_if_ill";
							settings.pr_tested_if_ill =
								static_cast<double>(
									values.at(val));
							val = "beta_S_to_E";
							settings.beta_S_to_E =
								static_cast<double>(
									values.at(val));
							val = "nu_E_to_A";
							settings.nu_E_to_A =
								static_cast<double>(
									values.at(val));
							val = "alpha_A_to_I";
							settings.alpha_A_to_I =
								static_cast<double>(
									values.at(val));
							val = "mu_I_to_H";
							settings.mu_I_to_H =
								static_cast<double>(
									values.at(val));
							val = "rho_I_to_R";
							settings.rho_I_to_R =
								static_cast<double>(
									values.at(val));
							val = "zeta_H_to_R";
							settings.zeta_H_to_R =
								static_cast<double>(
									values.at(val));
							val = "eta_A_to_R";
							settings.eta_A_to_R =
								static_cast<double>(
									values.at(val));
							val = "gamma_R_to_S";
							settings.gamma_R_to_S =
								static_cast<double>(
									values.at(val));
							val = "close_strat_t";
							settings.close.strat_t =
								static_cast<double>(
									values.at(val));
							val = "close_strat_general_weak_dist";
							settings.close.strat_general_weak_dist =
								static_cast<dist_type>(
									values.at(val));
							val = "close_strat_caregiver_weak_dist";
							settings.close.strat_caregiver_weak_dist =
								static_cast<dist_type>(
									values.at(val));
							val = "close_strat_client_weak_dist";
							settings.close.strat_client_weak_dist =
								static_cast<dist_type>(
									values.at(val));
							val = "close_strat_essential_weak_dist";
							settings.close.strat_essential_weak_dist =
								static_cast<dist_type>(
									values.at(val));
							val = "close_strat_caregiver_wears_ppe";
							settings.close.strat_caregiver_wears_ppe =
								static_cast<bool>(
									values.at(val));
							val = "close_strat_client_wears_ppe";
							settings.close.strat_client_wears_ppe =
								static_cast<bool>(
									values.at(val));
							val = "close_strat_essentials_wear_ppe";
							settings.close.strat_essentials_wear_ppe =
								static_cast<bool>(
									values.at(val));
							val = "close_strat_all_weak_ppe";
							settings.close.strat_all_weak_ppe =
								static_cast<bool>(
									values.at(val));
							val = "open_strat_t";
							settings.open.strat_t =
								static_cast<double>(
									values.at(val));
							val = "open_strat_general_weak_dist";
							settings.open.strat_general_weak_dist =
								static_cast<dist_type>(
									values.at(val));
							val = "open_strat_caregiver_weak_dist";
							settings.open.strat_caregiver_weak_dist =
								static_cast<dist_type>(
									values.at(val));
							val = "open_strat_client_weak_dist";
							settings.open.strat_client_weak_dist =
								static_cast<dist_type>(
									values.at(val));
							val = "open_strat_essential_weak_dist";
							settings.open.strat_essential_weak_dist =
								static_cast<dist_type>(
									values.at(val));
							val = "open_strat_caregiver_wears_ppe";
							settings.open.strat_caregiver_wears_ppe =
								static_cast<bool>(
									values.at(val));
							val = "open_strat_client_wears_ppe";
							settings.open.strat_client_wears_ppe =
								static_cast<bool>(
									values.at(val));
							val = "open_strat_essentials_wear_ppe";
							settings.open.strat_essentials_wear_ppe =
								static_cast<bool>(
									values.at(val));
							val = "open_strat_all_weak_ppe";
							settings.open.strat_all_weak_ppe =
								static_cast<bool>(
									values.at(val));
						}
						catch (...) { // if an at fails
							std::cerr << "Did not initialize " << val << '\n';
							throw;
						}

						if (settings.general_percent < 0) { // can't have negative percents
							throw std::runtime_error("by exclusion general percent is negative");
						}

						return settings;
					}


					void run_simulations(const std::string& file_base, int file_id_low, int file_id_high, const graph::graph_structure& settings,
						double dT, int num_iter) {

						thread_local std::ofstream fout;

						// generate files over a given range
						for (int id = file_id_low; id <= file_id_high; ++id) {

							covid::graph population{ settings };

							population.initialize();

							std::ostringstream oss;
							oss << file_base << id << ".txt";

							fout.open(oss.str());
							population.run(dT, num_iter, fout);
							fout.close();

						}
					}

					// pure virtual destructor definition
					initial_infected::~initial_infected() {}

					// other initial infected derived class definitions

					fraction_infected::fraction_infected(double _frac) : fraction(_frac) {}

					fraction_infected::operator double() const { return fraction; }

					numbers_infected::numbers_infected(std::vector<size_t> 			nums_asymptomatic, std::vector<size_t> nums_ill) :
						numbers_asymptomatic(std::move(nums_asymptomatic)),
						numbers_ill(std::move(nums_ill)) {}

					const std::vector<size_t>&
						numbers_infected::get_vec_asymptomatic() const { return numbers_asymptomatic; }

					const std::vector<size_t>& numbers_infected::get_vec_ill() const { return numbers_ill; }


}