comp = g++ -std=c++17 -O3 -c
link = g++ -lm

covid: Source.o Graph.o
	$(link) Source.o Graph.o -o covid -pthread
	
Source.o: CovidGraph.h CovidGraph.cpp 
	$(comp) Source.cpp -o Source.o
	
Graph.o: CovidGraph.h CovidGraph.cpp 
	$(comp) CovidGraph.cpp -o Graph.o