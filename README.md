## BACKGROUND

This code accompanies the manuscript "Networks of Necessity: Simulating Strategies for COVID-19 Mitigation among Disabled People and Their Caregivers" by Thomas E. Valles, Hannah Shoenhard, Joseph Zinski, Sarah Trick, Mason A. Porter, and Michael R. Lindstrom.

The code was written in 2020 by Michael R. Lindstrom and Thomas E. Valles. This is version 2.0 and may be updated as necessary.

## GETTING STARTED

To get started, create a new directory and place Source.cpp, CovidGraph.h, CovidGraph.cpp, parameters.txt, and makefile inside of it.

If on Linux with GNU C++17 available and make installed, move to that directory and type 'make'. The program will be compiled and linked for you. You can then run it with "./covid".

On Windows or with other compilers, you need to compile with the C++17 Standard OR IT WILL NOT COMPILE! Your compiler must accept the C++17 Standard!

Note that you can change the settings when you run the program through the command line (as stated in Source.cpp). The default settings here will produce 4 simulations running to 300 days, reading from "parameters.txt" and writing to files "sim[i].txt" where i=0, 1, 2, 3.